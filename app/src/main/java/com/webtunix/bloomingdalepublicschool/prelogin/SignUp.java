package com.webtunix.bloomingdalepublicschool.prelogin;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.basgeekball.awesomevalidation.utility.RegexTemplate;
import com.google.android.material.textfield.TextInputEditText;
import com.webtunix.bloomingdalepublicschool.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.concurrent.ExecutionException;

public class SignUp extends AppCompatActivity {

    Spinner spinner;
    ArrayAdapter<String> arrayAdapter;
    ArrayList<String> arrayList=null;
    ArrayList<Integer> spinnerId = null;
    String current="";
    StringBuilder total;
    String responseString="false";
    int responseCode =0;
    TextInputEditText  username, email,password,confirmpassword,phonenumber,countryCode, firstName, lastName, cityName, confirmEmail;
    AwesomeValidation awesomeValidation;
    AlertDialog alertDialog;
    String useremailString,userString,userPasswordString,confirmpasswordString,phonenumberString, firstNameString, lastNameString, cityNameString;
    String spinnerValue;

    String contryId = null;
    String contryDialCode = null;
    String country = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_sign_up);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        arrayList = new ArrayList<String>();
        spinnerId = new ArrayList<Integer>();


        spinner = findViewById(R.id.spinner);

        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);

        confirmpassword = findViewById(R.id.confirmpasswprd);
        phonenumber = findViewById(R.id.phonenumber);

        countryCode = findViewById(R.id.countrycode);

        firstName = findViewById(R.id.firstName);
        lastName = findViewById(R.id.lastName);
        cityName = findViewById(R.id.cityName);
        confirmEmail = findViewById(R.id.confirmemail);



        username.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(username.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(username.getApplicationWindowToken(), 0);

                }
            }
        });
        email.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(email.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(email.getApplicationWindowToken(), 0);

                }
            }
        });
        password.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(password.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(password.getApplicationWindowToken(), 0);

                }
            }
        });
        confirmpassword.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(confirmpassword.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(confirmpassword.getApplicationWindowToken(), 0);

                }
            }
        });
        phonenumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(phonenumber.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(phonenumber.getApplicationWindowToken(), 0);

                }
            }
        });
        firstName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(firstName.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(firstName.getApplicationWindowToken(), 0);

                }
            }
        });
        lastName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(lastName.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(lastName.getApplicationWindowToken(), 0);

                }
            }
        });
        cityName.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(cityName.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(cityName.getApplicationWindowToken(), 0);

                }
            }
        });
        confirmEmail.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(confirmEmail.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(confirmEmail.getApplicationWindowToken(), 0);

                }
            }
        });



        awesomeValidation.addValidation(this, R.id.email, android.util.Patterns.EMAIL_ADDRESS, R.string.err_email);
        awesomeValidation.addValidation(this, R.id.confirmemail, R.id.email, R.string.err_email_confirmation);

        awesomeValidation.addValidation(this, R.id.firstName, RegexTemplate.NOT_EMPTY, R.string.errorfirstName);
        awesomeValidation.addValidation(this, R.id.firstName, "[a-zA-Z\\s-]+", R.string.err_firstnamealpha);


        awesomeValidation.addValidation(this, R.id.lastName, RegexTemplate.NOT_EMPTY, R.string.errorlastName);
        awesomeValidation.addValidation(this, R.id.lastName, "[a-zA-Z\\s-]+", R.string.err_name);


        awesomeValidation.addValidation(this, R.id.password, "[\\S]{8,}", R.string.invalid_password);
     //   awesomeValidation.addValidation(this, R.id.password, RegexTemplate.NOT_EMPTY, R.string.err_password);
    //    awesomeValidation.addValidation(this, R.id.confirmpasswprd, RegexTemplate.NOT_EMPTY, R.string.err_password);

      //  awesomeValidation.addValidation(this, R.id.confirmpasswprd, "[a-zA-Z0-9!@#$%^&*()\\\\s-]{0,19}+", R.string.err_password);

        awesomeValidation.addValidation(this, R.id.username, "\\S+", R.string.err_noname);
      //  awesomeValidation.addValidation(this, R.id.username, RegexTemplate.NOT_EMPTY, R.string.err_name);

        awesomeValidation.addValidation(this, R.id.confirmpasswprd, R.id.password, R.string.err_password_confirmation);

        awesomeValidation.addValidation(this, R.id.cityName, RegexTemplate.NOT_EMPTY, R.string.errorcity);

        awesomeValidation.addValidation(this, R.id.phonenumber, "^[+]?[0-9]{10,13}$", R.string.err_tel);
        awesomeValidation.disableAutoFocusOnFirstFailure();
        Log.i("HELLO ARRAYLIST AAYI ", "I am here check");


        TelephonyManager telephonyMngr = (TelephonyManager) this.getSystemService(Context.TELEPHONY_SERVICE);

        contryId = telephonyMngr.getSimCountryIso().toUpperCase();
        String[] arrContryCode=this.getResources().getStringArray(R.array.DialingCountryCode);
        for(int i=0; i<arrContryCode.length; i++){
            String[] arrDial = arrContryCode[i].split(",");
            if(arrDial[1].trim().equals(contryId.trim())){
                contryDialCode = arrDial[0];
                country = arrDial[1];
            }
        }

        Log.i("Getting Country  ", country);

        countryCode.setText("+"+contryDialCode);
        new DoSomething().execute();

    }

    public void openSignIn(View view) {

        Intent intent = new Intent(this,SignIn.class);
        startActivity(intent);


    }

    public void onSubmit(View view) {



if(awesomeValidation.validate()){
    userString = username.getText().toString().trim();
    useremailString = email.getText().toString();
    userPasswordString = password.getText().toString();
    confirmpasswordString = confirmpassword.getText().toString();
    phonenumberString = "+"+contryDialCode.trim()+phonenumber.getText().toString();
    spinnerValue=""+spinnerId.get(spinner.getSelectedItemPosition());
    firstNameString = firstName.getText().toString();
    lastNameString = lastName.getText().toString();
    cityNameString = cityName.getText().toString();




    Log.d("BEHEJNA",""+userString+":"+useremailString+":"+spinnerValue+""+spinnerId.toString());



    UploadValues uploadValues = new UploadValues();

    try {
        responseString = uploadValues.execute().get();
    } catch (ExecutionException e) {
        e.printStackTrace();
    } catch (InterruptedException e) {
        e.printStackTrace();
    }

    Log.i("Vineet Kumar G Hun ", responseString + "");
    if(responseString.trim().equals("200")){


AlertDialog alertDialog = new AlertDialog.Builder(SignUp.this).create();
        alertDialog.setTitle("Message");
        alertDialog.setMessage("Your account is successfully created, Please sign in with credentials" +
                "\n" +
                "");
        alertDialog.setIcon(R.mipmap.logo);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {

                Toast.makeText(SignUp.this,"Account created Successfully ",Toast.LENGTH_SHORT).show();

                Intent intent = new Intent(SignUp.this, SignIn.class);

                startActivity(intent);

            }
        });

        alertDialog.show();

    }else{




        AlertDialog alertDialog = new AlertDialog.Builder(SignUp.this).create();
        alertDialog.setTitle("Sign Up alert");
        alertDialog.setMessage("Username or Email Id already exists");
        alertDialog.setIcon(R.mipmap.logo);

        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {


            }
        });

        alertDialog.show();




    }


}




    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();

        Intent intent = new Intent(SignUp.this,SignIn.class);
        startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();
        finish();
    }

    class DoSomething extends AsyncTask<Void,Void,Void>{


        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL obj = new URL(getString(R.string.server_link)+"Main_Course_Category_list?format=json");

               HttpURLConnection con = (HttpURLConnection) obj.openConnection();
               con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
               int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }
 
                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
         try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    arrayList.add(jsonObject2.optString("title").toString());
                    spinnerId.add((jsonObject2.optInt("id")));

                    Log.d("HELLO ARRAYLIST AAYI ",arrayList.toString());

                }

            }catch (Exception e){

            }

            arrayAdapter = new ArrayAdapter<String>(SignUp.this,android.R.layout.simple_spinner_item, arrayList);
           // arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

            spinner.setAdapter(arrayAdapter);




        }
    }



    class UploadValues extends AsyncTask<Void,Void,String> {


        @Override
        protected String doInBackground(Void... voids) {

            try {
                URL obj = new URL(getString(R.string.server_link)+"Singup");



                JSONObject postDataParams = new JSONObject();
                postDataParams.put("first_name",firstNameString.trim());
                postDataParams.put("last_name",lastNameString.trim());
                postDataParams.put("username",userString.trim());
                postDataParams.put("email", useremailString.trim());
                postDataParams.put("main_course_category_id", spinnerValue.trim());
                postDataParams.put("mobile", phonenumberString);
                postDataParams.put("city", cityNameString.trim());
                postDataParams.put("country", country.trim());

                postDataParams.put("password1", userPasswordString);
                postDataParams.put("password2", confirmpasswordString);



                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setReadTimeout(15000 /* milliseconds */);
                con.setConnectTimeout(15000 /* milliseconds */);
                con.setRequestMethod("POST");
                con.setDoOutput(true);

                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "utf-8"));

                writer.write(getPostDataString(postDataParams));
                Log.i("paramsData VINIIIII", getPostDataString(postDataParams).toString());


                writer.flush();
                writer.close();
                os.close();

                responseCode = con.getResponseCode();


                System.out.println("Response Code :: " + responseCode);

                Log.i(" Sir Java ", responseCode + "");


            } catch (Exception e) {
                e.printStackTrace();
            }
            return responseCode+"";
        }
        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while(itr.hasNext()){

                String key= itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            Log.i("Vineet Kumar G Ki aaya", result.toString() + "");

            return result.toString();
        }

    }


}
