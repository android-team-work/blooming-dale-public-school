package com.webtunix.bloomingdalepublicschool.prelogin;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;


import com.webtunix.bloomingdalepublicschool.R;

import java.util.ArrayList;

public class PaymentAdapter extends  RecyclerView.Adapter<PaymentAdapter.ViewHolder> {

    PaymentRadio paymentRadio [];
    ArrayList<String> planNamee = new ArrayList<>();
    ArrayList<String> price = new ArrayList<>();
    ArrayList<String> days= new ArrayList<>();
    ArrayList<Integer> spinnerId= new ArrayList<>();
    PaymentActivity  paymentActivity;
    private RadioButton lastCheckedRB = null;
    Context context;
RadioGroup radioGroup;

    public PaymentAdapter( Context context,
                           ArrayList<String> planName,ArrayList<Integer> spinnerId,

                                   ArrayList<String> days, ArrayList<String> price,RadioGroup radioGroup
                                   ) {


       paymentActivity = new PaymentActivity();
this.context = context;

       this.planNamee = planName;
       this.price = price;
       this.days = days;
       this.spinnerId = spinnerId;
this.radioGroup = radioGroup;
    }



    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.paymentplanrecycler, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;

    }

    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

      //      paymentRadio[position] = new PaymentRadio();
        /*  paymentRadio[position].setValue(false);*/



        holder.planName.setText("Plan : "+planNamee.get(position).toString());
        holder.planDays.setText(" Valid For : "+days.get(position)+" Days");
        holder.planPrice.setText("Price (INR) : "+price.get(position));

holder.radioButton.setId(position);


        Log.d("Module Subcategory ","I am here only");



    }

    @Override
    public int getItemCount() {
        return spinnerId.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView planName, planPrice, planDays;
int value=-1;
        int itemPOistion;
        RadioButton radioButton;


        public ViewHolder(@NonNull final View itemView) {
            super(itemView);


            planName = itemView.findViewById(R.id.planNameTextView);

            planPrice = itemView.findViewById(R.id.planPriceTextView);

            planDays  = itemView.findViewById(R.id.planDaysTextView);
            radioButton = itemView.findViewById(R.id.checkradioforpayment);

            radioButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    itemPOistion = getAdapterPosition();
                    Log.d("RADIOTG", "Button ID" + radioButton.getId());


                PaymentActivity.getValue(itemPOistion);
                }
            });

            radioButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

                    radioGroup.clearCheck();

                    if (isChecked) {
                        radioGroup.check(buttonView.getId());
                    }
                }
            });
        }
        @Override
        public void onClick(View view) {
        }



    }
}
