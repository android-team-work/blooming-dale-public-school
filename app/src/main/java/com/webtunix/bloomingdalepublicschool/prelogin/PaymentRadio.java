package com.webtunix.bloomingdalepublicschool.prelogin;

public class PaymentRadio {

    private Boolean value;

    PaymentRadio(){

    }

    public Boolean getValue() {
        return value;
    }


    public void setValue(Boolean value) {
        this.value = value;
    }
}
