package com.webtunix.bloomingdalepublicschool.adminmodule.adminuserdetail;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.webtunix.bloomingdalepublicschool.R;
import com.webtunix.bloomingdalepublicschool.adminmodule.adminsearchlist.AdminSearchListAdapter;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Iterator;

public class AdminUserDeatilFragment extends Fragment {

    AdminSearchListAdapter adminSearchListAdapter;

    ArrayList<Bitmap> bitmaps;
    RecyclerView recyclerView;

  String emailUser;

  String phonenumber,exp_date;
int userType, user_ID;
int  permanent = -1;

ArrayList<String> batchName;
ArrayList<Integer> batchID;

TextView phoneview, emailView,validity;

RadioButton  studentradio,techerradio;

RadioGroup radioGroup;

CardView cardView;
Button uploadButton;
    StringBuilder total, subTotal;

    DatePicker datePicker;
    View root;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        root = inflater.inflate(R.layout.activity_admin_user_deatil_fragment, container, false);
        batchName = new ArrayList<>();
       batchID = new ArrayList<>();

       studentradio =  root.findViewById(R.id.studentradio);
        techerradio =  root.findViewById(R.id.techerradio);

        datePicker = root.findViewById(R.id.datePicker1adminSingleUserList);
        uploadButton = root.findViewById(R.id.uploadEditUser);

        phoneview = root.findViewById(R.id.adminsingleuserPhoneView);
        emailView = root.findViewById(R.id.adminsingleuseremailView);
        validity = root.findViewById(R.id.expiration_Validity);
        radioGroup = root.findViewById(R.id.adminsingleuserlistradioGroup);

         cardView = root.findViewById(R.id.adminCardViewusersinglelist);

       new DoSomething().execute();
   //   new LoadBacthes().execute();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            datePicker.setOnDateChangedListener(new DatePicker.OnDateChangedListener() {
                @Override
                public void onDateChanged(DatePicker view, int yeear, int monthOfYear, int dayOfMonth) {

                    Calendar date = Calendar.getInstance();

                    int currentday = date.get(Calendar.DAY_OF_MONTH);

                    int currentMonth = date.get(Calendar.MONTH)+1;

                    int currentYear = date.get(Calendar.YEAR);

                    Log.d("Current ","day"+currentday+"month"+currentMonth);
                    int day,month,year;




                    day = datePicker.getDayOfMonth();
                    month = datePicker.getMonth()+1;
                    year = datePicker.getYear();
                    Log.d("datepicker ","day"+day+"month"+year);

                    if((year>=currentYear)){

                        if((year==currentYear)&& (month>=currentMonth)) {




                              if((year==currentYear)&& (month>currentMonth)){


                                String properFormat = year+"-"+month+"-"+day;
                                exp_date = properFormat;
                            }else

                            if(day>=currentday){


                                uploadButton.setEnabled(true);


                                String properFormat = year+"-"+month+"-"+day;

                                exp_date = properFormat;

                            }else{
                                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                                alertDialog.setTitle("Date Picker Alert");
                                alertDialog.setMessage("Please Select Current Date Or Coming Date of the month.");
                                alertDialog.setIcon(R.mipmap.logo);

                                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int which) {

                                        uploadButton.setEnabled(false);

                                    }
                                });

                                alertDialog.show();
                                uploadButton.setEnabled(false);
                            }



                        }else if((year>currentYear)){


                            String properFormat = year+"-"+month+"-"+day;
                            exp_date = properFormat;
                        }else{
                            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                            alertDialog.setTitle("Date Picker Alert");
                            alertDialog.setMessage("Please Select Current Month Or Coming Month.");
                            alertDialog.setIcon(R.mipmap.logo);

                            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {
                                    uploadButton.setEnabled(false);


                                }
                            });

                            alertDialog.show();
                            uploadButton.setEnabled(false);

                        }

                        uploadButton.setEnabled(true);

                    }else {

                        AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                        alertDialog.setTitle("Date Picker Alert");
                        alertDialog.setMessage("Please Select Current Year Or Coming Year.");
                        alertDialog.setIcon(R.mipmap.logo);

                        alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int which) {

                                uploadButton.setEnabled(false);

                            }
                        });

                        alertDialog.show();
                        uploadButton.setEnabled(false);

                    }
                }
            });
        }
        datePicker.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {




            }
        });


        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {

               switch(checkedId){

                   case R.id.techerradio:

                       permanent =1;
                       uploadButton.setEnabled(true);

                       break;

                   case R.id.studentradio:
                       permanent =2;
                       uploadButton.setEnabled(true);

                       break;

                }


            }
        });

        uploadButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if((userType==1)||(userType==3)){
                    AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                    alertDialog.setTitle("Date Picker Alert");
                    alertDialog.setMessage("You Cannot Upload Any Data. \n Please Press Back Button Or Home Button ");
                    alertDialog.setIcon(R.mipmap.logo);

                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                        }
                    });

                    alertDialog.show();
                    uploadButton.setEnabled(false);
                }else {

                    try {
                        String responsee = new UploadingUserInfo().execute().get();


                        if (responsee.trim().equals("200")) {

Log.d("Bhaji uploading","Hanji");

                            Log.d("Bhaji permanent","Hanji"+permanent);

                            if (permanent == 2) {
                                Bundle bundle = new Bundle();


                                bundle.putString("URL", getString(R.string.server_link) + "Student_list");
                                bundle.putString("UserType","Student");


                           //     Navigation.findNavController(root).navigate(R.id.adminsearchlist, bundle);

                                NavController controller = Navigation.findNavController(root);
                                controller.popBackStack(R.id.adminUserDeatilFragment, true);
                                controller.popBackStack(R.id.adminsearchlist, true);


                                controller.navigate(R.id.adminsearchlist, bundle);

                            } else if (permanent == 0) {
                                Bundle bundle = new Bundle();
                                bundle.putString("URL", getString(R.string.server_link) + "Guest_list");
                                bundle.putString("UserType","Guest");

                            //    Navigation.findNavController(root).navigate(R.id.adminsearchlist, bundle);

                                NavController controller = Navigation.findNavController(root);
                                controller.popBackStack(R.id.adminUserDeatilFragment, true);
                                controller.popBackStack(R.id.adminsearchlist, true);

                                controller.navigate(R.id.adminsearchlist, bundle);
                            }else if (permanent == 1) {
                                Bundle bundle = new Bundle();
                                bundle.putString("URL", getString(R.string.server_link) + "Teacher_list");
                                bundle.putString("UserType","Teacher");

                               // Navigation.findNavController(root).navigate(R.id.adminsearchlist, bundle);

                                NavController controller = Navigation.findNavController(root);
                                controller.popBackStack(R.id.adminUserDeatilFragment, true);
                                controller.popBackStack(R.id.adminsearchlist, true);

                                controller.navigate(R.id.adminsearchlist, bundle);

                            }else if (permanent == 3) {
                                Bundle bundle = new Bundle();
                                bundle.putString("URL", getString(R.string.server_link) + "Paid_list");
                                bundle.putString("UserType","Paid User");

                              //  Navigation.findNavController(root).navigate(R.id.adminsearchlist, bundle);
                                NavController controller = Navigation.findNavController(root);
                                controller.popBackStack(R.id.adminUserDeatilFragment, true);
                                controller.popBackStack(R.id.adminsearchlist, true);

                                controller.navigate(R.id.adminsearchlist, bundle);
                            }


                        } else {

                            Toast.makeText(getActivity(), "Something Is Wrong, Please Select Proper Values", Toast.LENGTH_SHORT).show();

                        }

                    } catch (Exception e) {

                    }
                }

            }
        });


        return root;
    }



    class DoSomething extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {


                         Log.d("USER SINGLE LIST",""+getArguments().getString("selectuser"));
                URL obj = new URL(getString(R.string.server_link)+"Update_user_list/"+getArguments().getString("selectuser"));

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                    emailUser=jsonObject2.optString("email").toString();

                    phonenumber=jsonObject2.optString("mobile").toString();
                    userType = jsonObject2.optInt("user_type");
                    user_ID = jsonObject2.optInt("user_id");
                    exp_date = jsonObject2.optString("exp_date");


/*

                    Log.d("HELLO ARRAYLIST AAYI ",arrayList.toString());
                    Log.d("HELLO ARRAYLIST AAYI ",imageurl.toString());
*/


                }

            }catch (Exception e){

            }

                      phoneview.setText(phonenumber);

                     emailView.setText(emailUser);

                     validity.setText(exp_date);


                       if((userType==0)||(userType==2)){

                           cardView.setEnabled(true);
                           studentradio.setVisibility(View.VISIBLE);
                           uploadButton.setEnabled(true);
                           datePicker.setEnabled(true);
                           techerradio.setEnabled(true);
                           techerradio.setChecked(false);

                           studentradio.setChecked(false);


                           if(userType ==2){
                                 studentradio.setChecked(true);

                             }





                       }else if((userType==1)||(userType==3)){


               //            Toast.makeText(getActivity(),"User Type "+userType,Toast.LENGTH_LONG).show();

                          datePicker.setEnabled(false);
                           uploadButton.setEnabled(false);

                           if(userType==1){
                               techerradio.setVisibility(View.VISIBLE);

                               techerradio.setChecked(true);
                               studentradio.setVisibility(View.GONE);

                           }else{
                               studentradio.setVisibility(View.VISIBLE);
                               techerradio.setVisibility(View.GONE);
                               studentradio.setChecked(true);


                           }
                                    uploadButton.setEnabled(false);
                                    cardView.setEnabled(false);

                       }


        }
    }



    class UploadingUserInfo extends AsyncTask<String,Void,String> {

        int responseCode;

        protected String doInBackground(String ...strings) {


            try {



                           if(permanent==-1){
                               permanent = 0;
                           }

                JSONObject postDataParams = new JSONObject();

                postDataParams.put("user_id", user_ID);

                postDataParams.put("exp_date", exp_date);

                postDataParams.put("user_type", permanent);



                URL obj = new URL(getString(R.string.server_link)+"User_Edit_Api");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");

                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");


                con.setRequestProperty("Expect", "100-continue");


                con.setDoOutput(true);
                //      con.setDoInput(true);
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "utf-8"));

                writer.write(getPostDataString(postDataParams));
                Log.i("paramsData VINIIIII FCM", getPostDataString(postDataParams).toString());


                writer.flush();
                writer.close();
                os.close();
                responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);



            }catch (Exception e){

            }
            return responseCode+"";

        }

        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while(itr.hasNext()){

                String key= itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            Log.i("Vineet Kumar G FCM", result.toString() + "");

            return result.toString();
        }
    }




}
