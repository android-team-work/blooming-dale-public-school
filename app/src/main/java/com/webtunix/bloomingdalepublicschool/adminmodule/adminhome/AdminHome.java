package com.webtunix.bloomingdalepublicschool.adminmodule.adminhome;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;

import androidx.cardview.widget.CardView;
import androidx.fragment.app.Fragment;
import androidx.navigation.Navigation;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.TextView;

import com.webtunix.bloomingdalepublicschool.R;


/**
 * A simple {@link Fragment} subclass.
 */
public class AdminHome extends Fragment {

    CardView  userList, studentList, teacherList, paiduserList;
    TextView notificationUpload;
    String isSuperUser;
    Button button;

    public AdminHome() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        SharedPreferences sh = getActivity().getSharedPreferences("techapppref", Context.MODE_PRIVATE);
        isSuperUser = sh.getString("superUser", "");


     if (isSuperUser.trim().equals("false")) {
            View view = inflater.inflate(R.layout.fragment_teacher_home, container, false);
            notificationUpload = view.findViewById(R.id.uploadTeacherNotfication);
           button = view.findViewById(R.id.teacherCourseButton);

         button.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Navigation.findNavController(v).navigate(R.id.nav_home);

             }
         });
         notificationUpload.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {


                 Navigation.findNavController(view).navigate(R.id.adminuploadnotification);




             }
         });
            return view;


        } else {
            View root = inflater.inflate(R.layout.fragment_admin_home, container, false);

            notificationUpload = root.findViewById(R.id.uploadNotfication);

            userList = root.findViewById(R.id.guestlistbtn);

            studentList = root.findViewById(R.id.studentlistbtn);

            teacherList = root.findViewById(R.id.teacherlistbtn);

            paiduserList = root.findViewById(R.id.paiduserlistbtn);


            notificationUpload.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Navigation.findNavController(view).navigate(R.id.adminuploadnotification);

                }
            });
            userList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Bundle bundle = new Bundle();
                    bundle.putString("URL", getString(R.string.server_link) + "Guest_list");
                    bundle.putString("UserType", "Guest");
                    Navigation.findNavController(view).getGraph().findNode(R.id.adminsearchlist).setLabel("Guest List");

                    Navigation.findNavController(view).navigate(R.id.adminsearchlist, bundle);

                }
            });
            studentList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Bundle bundle = new Bundle();
                    bundle.putString("URL", getString(R.string.server_link) + "Student_list");
                    bundle.putString("UserType", "Student");
                    Navigation.findNavController(view).getGraph().findNode(R.id.adminsearchlist).setLabel("Student List");

                    Navigation.findNavController(view).navigate(R.id.adminsearchlist, bundle);


                }
            });
            teacherList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                    Bundle bundle = new Bundle();
                    bundle.putString("URL", getString(R.string.server_link) + "Teacher_list");
                    bundle.putString("UserType", "Teacher");
                    Navigation.findNavController(view).getGraph().findNode(R.id.adminsearchlist).setLabel("Teacher List");

                    Navigation.findNavController(view).navigate(R.id.adminsearchlist, bundle);


                }
            });
            paiduserList.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    Bundle bundle = new Bundle();
                    bundle.putString("URL", getString(R.string.server_link) + "Paid_list");
                    bundle.putString("UserType", "Paid User");
                    Navigation.findNavController(view).getGraph().findNode(R.id.adminsearchlist).setLabel("Paid User List");

                    Navigation.findNavController(view).navigate(R.id.adminsearchlist, bundle);

                }
            });

            return root;
        }
    }
}
