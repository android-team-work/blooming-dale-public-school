package com.webtunix.bloomingdalepublicschool.adminmodule.activities;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

import com.webtunix.bloomingdalepublicschool.R;


public class WelcomeAdmin extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_welcome_admin);
    }
}


