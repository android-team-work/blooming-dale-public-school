package com.webtunix.bloomingdalepublicschool.adminmodule.adminnotification;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface NotificationApi {


    String BASE_URL = "http://www.bloomingdalepublicschool.com/api/v4/";

    @Multipart

    @POST("Uplode_noti/")
   Call<MyResponse> uploaddImage(@Part("batch_id") Integer batch_id, @Part("title") String title
                              , @Part("details") String details,  @Part MultipartBody.Part file);


   /* Call<MyResponse> uploaddImage(@Part("title") String title,
                                  @Part MultipartBody.Part file, @Part("details") String details,


                                   @Part("batch_id")Integer batch_id);

*/
}



