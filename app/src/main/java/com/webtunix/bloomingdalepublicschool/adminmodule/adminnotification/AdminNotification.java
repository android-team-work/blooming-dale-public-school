package com.webtunix.bloomingdalepublicschool.adminmodule.adminnotification;

import android.Manifest;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.loader.content.CursorLoader;
import androidx.navigation.Navigation;

import android.provider.MediaStore;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.webtunix.bloomingdalepublicschool.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;


public class AdminNotification extends Fragment {
    private static final int SELECT_PICTURE = 100;

    ArrayList<String> batchName;

    ArrayList<Integer> batchId;


    ArrayList<String> mainCategoryNames;
    ArrayList<Integer> mainCategoryID;

    ArrayList<String> subCategoryNames;
    ArrayList<Integer> subCategoryID;


    ArrayList<String> innerSubCategoryNames;
    ArrayList<Integer> innerSubCategoryID;

    TextView selectMainTextView, selectedsubTextView, selectedInnerSubTextView ;


    int selectedMainCategory,selectedSubCategory, selectedInnerSubCategory;

    private int CAMREA_CODE = 1;
    Bitmap bitmap;
    int uploadBatchID;
    String imagePath,title=null,description = null;

    TextView notificationMainTitle;

   TextView uploadNotificationImagePath = null;

    Button enterNotificationButton;

    Spinner uploadBatches;
    EditText notificationDescription;

    StringBuilder total,getMainCategoryBuilder,getSubCategoryBuilder,getInnerSubCategoryBuilder;

    ArrayAdapter arrayAdapter,mainCategoryArrayAdapter,subCategoryArrayAdapter,innerSubCategoryArrayAdapter;
    View root;
    String s1,pass,str;
    Uri selectedFile;

    int counter=0;
MyResponse  myResponse;

    Gson gson;
    Retrofit retrofit1;

    CheckBox  checkboxWebcontent;

    LinearLayout bottomNavigationView;
    ScrollView scrollView;

Spinner mainCategory, subcategory, innerSubCategory;


    public AdminNotification() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment

        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        root = inflater.inflate(R.layout.fragment_admin_notification, container, false);



        SharedPreferences sh = getActivity().getSharedPreferences("techapppref",Context.MODE_PRIVATE);

        s1 = sh.getString("useremail", "");
        pass = sh.getString("userpassword", "");

        //The gson builder
         gson = new GsonBuilder()
                .setLenient()
                .create();


        //creating retrofit object
        retrofit1 = new Retrofit.Builder()
                .baseUrl(NotificationAsWebApi.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        str = s1.trim()+":"+pass.trim();
         batchName = new ArrayList<>();

         batchId = new ArrayList<>();

       mainCategoryNames = new   ArrayList<String>();
        mainCategoryID = new   ArrayList<Integer>();

        subCategoryNames= new   ArrayList<String>();
     subCategoryID = new   ArrayList<Integer>();


         innerSubCategoryNames = new   ArrayList<String>();
         innerSubCategoryID = new   ArrayList<Integer>();

        selectMainTextView = root.findViewById(R.id.selectMainCategory);

        selectedsubTextView = root.findViewById(R.id.selectSubCategory);

        selectedInnerSubTextView = root.findViewById(R.id.selectInnerSubCategory);

        bottomNavigationView = (LinearLayout) getActivity().findViewById(R.id.bottomViewRealtiveLayout);
scrollView = root.findViewById(R.id.uplodNotificationScrolview);
        notificationMainTitle = root.findViewById(R.id.adminnotificationMainTitle);
enterNotificationButton = root.findViewById(R.id.uploadtotalNotificationButton);

uploadBatches = root.findViewById(R.id.notificationspinnerofadminBatch);

notificationDescription = root.findViewById(R.id.adminnotificationdescription);

uploadNotificationImagePath = root.findViewById(R.id.uploadnotificationImage);


checkboxWebcontent = root.findViewById(R.id.adminNotificationsavecheckbox);

mainCategory = root.findViewById(R.id.adminnotificationMainCategory);
subcategory = root.findViewById(R.id.adminnotificationsubCategory);
innerSubCategory = root.findViewById(R.id.adminnotificationinnersubCategory);

mainCategory.setEnabled(false);
subcategory.setEnabled(false);
innerSubCategory.setEnabled(false);


        notificationDescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                bottomNavigationView.setVisibility(View.GONE);
            }
        });
        notificationDescription.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                bottomNavigationView.setVisibility(View.GONE);

                int viewHeight = scrollView.getMeasuredHeight();
                int contentHeight = scrollView.getChildAt(0).getHeight();
                if(viewHeight - contentHeight < 0) {

                    bottomNavigationView.setVisibility(View.GONE);

                }else{
                    bottomNavigationView.setVisibility(View.VISIBLE);
                }


            }
        });

mainCategory.setVisibility(View.GONE);
        subcategory.setVisibility(View.GONE);
        innerSubCategory.setVisibility(View.GONE);
        selectMainTextView.setVisibility(View.GONE);

        selectedsubTextView.setVisibility(View.GONE);
        selectedInnerSubTextView.setVisibility(View.GONE);



        new UserBatches().execute();



        notificationMainTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(notificationMainTitle.getText()==null)){
                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(notificationMainTitle.getApplicationWindowToken(), 0);

                }
            }
        });
      /*  notificationMainTitle.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {

                if(!(notificationMainTitle.getText()==null)){

                    InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(notificationMainTitle.getApplicationWindowToken(), 0);

                }
            }
        });
*/
    checkboxWebcontent.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View v) {


        if(checkboxWebcontent.isChecked()){

            mainCategory.setVisibility(View.VISIBLE);


            selectMainTextView.setVisibility(View.VISIBLE);
            mainCategory.setEnabled(true);


                new GetMainCategory().execute();



        }else{


            selectedMainCategory=-1;
            selectedSubCategory =-1;
            selectedInnerSubCategory=-1;

            mainCategory.setEnabled(false);
            subcategory.setEnabled(false);
            innerSubCategory.setEnabled(false);
            mainCategory.setVisibility(View.GONE);
            subcategory.setVisibility(View.GONE);
            innerSubCategory.setVisibility(View.GONE);
            selectMainTextView.setVisibility(View.GONE);

            selectedsubTextView.setVisibility(View.GONE);
            selectedsubTextView.setVisibility(View.GONE);
        }

    }
});





    mainCategory.setOnTouchListener(new View.OnTouchListener() {
    @Override
    public boolean onTouch(View v, MotionEvent event) {

        selectedMainCategory =  mainCategoryID.get(mainCategory.getSelectedItemPosition());
        subcategory.setEnabled(true);

        subcategory.setVisibility(View.VISIBLE);
        selectedsubTextView.setVisibility(View.VISIBLE);



            new GetSubCategory().execute(selectedMainCategory);



        return false;
    }
});


        subcategory.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {


                selectedSubCategory = subCategoryID.get(subcategory.getSelectedItemPosition());
                innerSubCategory.setEnabled(true);
                selectedInnerSubTextView.setVisibility(View.VISIBLE);

                innerSubCategory.setVisibility(View.VISIBLE);


                    new GetInnerSubCategory().execute(selectedSubCategory);



                return false;
            }
        });

        innerSubCategory.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {


               selectedInnerSubCategory = innerSubCategoryID.get(innerSubCategory.getSelectedItemPosition());





            }





            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        uploadNotificationImagePath.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*
*/ if (permissionAlreadyGranted()) {
                    Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, 100);
                }

                requestPermission();
            }



        });

    enterNotificationButton.setOnClickListener(new View.OnClickListener() {
    @Override
    public void onClick(View view) {

        if(notificationDescription.getText().toString().trim().equals("")
                ||uploadNotificationImagePath.getText().toString().trim().equals("")
                ||notificationMainTitle.getText().toString().trim().equals("")) {


            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Admin Notification Alert");
            alertDialog.setMessage("Please Fill All Fields & Upload Image");
            alertDialog.setIcon(R.mipmap.logo);

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {


                }
            });

            alertDialog.show();        }
        else {




      if(counter==0) {
            description = notificationDescription.getText().toString();
            uploadBatchID = uploadBatches.getSelectedItemPosition();
            title = notificationMainTitle.getText().toString();




            if((selectedMainCategory!=-1)&&(selectedSubCategory!=-1)&&(selectedInnerSubCategory!=-1)){



             uploadFileAsWebContent(selectedFile);



            }

            else{

                AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
                alertDialog.setTitle("Admin Notification Alert");
                alertDialog.setMessage("Please select All Categories Or Uncheck the Checkbox to upload notification");
                alertDialog.setIcon(R.mipmap.logo);

                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                alertDialog.show();
            }



            notificationDescription.setEnabled(false);
            notificationDescription.setFocusable(false);
            notificationDescription.setFocusableInTouchMode(false);
            notificationMainTitle.setEnabled(false);
            uploadNotificationImagePath.setEnabled(false);
            uploadBatches.setEnabled(false);
            enterNotificationButton.setEnabled(false);
           uploadFile(selectedFile);

       }else{
            Toast.makeText(getContext()," Please wait Button is Once Pressed",Toast.LENGTH_SHORT).show();

            notificationDescription.setEnabled(false);
            notificationMainTitle.setEnabled(false);
            notificationDescription.setFocusable(false);
            notificationDescription.setFocusableInTouchMode(false);
            uploadNotificationImagePath.setEnabled(false);
            uploadBatches.setEnabled(false);

        }
        }
//new DoSomething().execute(str);
    }
});

        return root;
    }

      public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 100 && resultCode == getActivity().RESULT_OK && data != null) {
            //the image URI
            selectedFile = data.getData();
            uploadNotificationImagePath.setText(uploadNotificationImagePath.toString());


            enterNotificationButton.setEnabled(true);


        }else{
            AlertDialog alertDialog = new AlertDialog.Builder(getActivity()).create();
            alertDialog.setTitle("Upload Picture Alert");
            alertDialog.setMessage("Please select an Image");
            alertDialog.setIcon(R.mipmap.logo);

            alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                public void onClick(DialogInterface dialog, int which) {


                }
            });

            alertDialog.show();

        }

        }
     private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(getActivity(), contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

     private void uploadFile(Uri fileUri) {

        //creating a file
        File file = new File(getRealPathFromURI(fileUri));

        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse(getActivity().getContentResolver().getType(fileUri)), file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestFile);
        Integer batch_ID = batchId.get(uploadBatches.getSelectedItemPosition());
       String title =  notificationMainTitle.getText().toString();

         List <Integer> list = new ArrayList();

         list.add(uploadBatchID);

        //The gson builder

         Log.d("Gettng All data","b: "+batch_ID+" t:"+title+"d:"+description);

        //creating our api
        NotificationApi api = retrofit1.create(NotificationApi.class);

        //creating a call and calling the upload image method
        Call<MyResponse> call = api.uploaddImage(batch_ID,title,description,fileToUpload);




      /*  Call
                <MyResponse> call = api.uploadData(batch_ID,title,"");*/

        //finally performing the call
        call.enqueue(new Callback<MyResponse>() {
            @Override
            public void onResponse(Call<MyResponse> call, Response<MyResponse> response) {

                Toast.makeText(getContext(),"Successfully Uploaded",Toast.LENGTH_SHORT).show();
              Log.d("Gettng response",""+response.message().toString());
                Log.d("Gettng response err",""+response.body());

                Navigation.findNavController(root).navigate(R.id.nav_adminhome);


            }

            @Override
            public void onFailure(Call<MyResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }


      private void uploadFileAsWebContent(Uri fileUri) {

        //creating a file
        File file = new File(getRealPathFromURI(fileUri));

        //creating request body for file
        RequestBody requestFile = RequestBody.create(MediaType.parse(getActivity().getContentResolver().getType(fileUri)), file);
        final MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("image", file.getName(), requestFile);

        String title =  notificationMainTitle.getText().toString();


        //The gson builder




        //creating our api
        NotificationAsWebApi api1 = retrofit1.create(NotificationAsWebApi.class);

        //creating a call and calling the upload image method
        Call<MyResponseAsWeb> call = api1.uploadddImage(selectedMainCategory,selectedSubCategory,selectedInnerSubCategory,title+"",fileToUpload,description);


        //finally performing the call
        call.enqueue(new Callback<MyResponseAsWeb>() {
            @Override
            public void onResponse(Call<MyResponseAsWeb> call, Response<MyResponseAsWeb> response) {

             //   Toast.makeText(getContext(),"Successfully Uploaded Notification As Web Content",Toast.LENGTH_SHORT).show();
        Log.d("Values categories","M"+selectedMainCategory+"S"+selectedSubCategory+"IN"+selectedInnerSubCategory+fileToUpload);
                Log.d("Gettng response cat",""+response.message());

             //   Navigation.findNavController(root).navigate(R.id.nav_adminhome);


            }

            @Override
            public void onFailure(Call<MyResponseAsWeb> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
     public String getPathFromURI(Uri contentUri) {
        String res = null;
        String[] proj = {MediaStore.Images.Media.DATA};
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        if (cursor.moveToFirst()) {
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            res = cursor.getString(column_index);
        }
        cursor.close();
        return res;
    }


     private boolean permissionAlreadyGranted() {

        int result = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);

        if (result == PackageManager.PERMISSION_GRANTED)
            return true;

        return false;
    }

    private void requestPermission() {

        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {

        }
        ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, CAMREA_CODE);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {

        if (requestCode == CAMREA_CODE) {

            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

            } else {
                boolean showRationale = shouldShowRequestPermissionRationale( Manifest.permission.READ_EXTERNAL_STORAGE );
                if (! showRationale) {

                }
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        bottomNavigationView.setVisibility(View.VISIBLE);
    }

    class DoSomething extends AsyncTask<String,Void,String> {

        int responseCode;
        String attachmentName = "bitmap";
        String attachmentFileName = "bitmap.bmp";
        String crlf = "\r\n";
        String twoHyphens = "--";
        String boundary =  "*****";

        @Override
        protected String doInBackground(String... strings) {

            try {

                byte[] data;
                String base64 = null;


                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }


                JSONObject postDataParams = new JSONObject();

                postDataParams.put("batch_id", "2");

                postDataParams.put("title", "Hello");

            //  postDataParams.put("image", bitmap);

                postDataParams.put("details", "Kuch bhi rakhlo");


                URL obj = new URL("http://ec2-18-188-51-40.us-east-2.compute.amazonaws.com/api/v1/message_send");



                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");
             /*   con.setRequestProperty("Content-Type", "application/json; charset=utf-8");

                con.setRequestProperty("Content-Type", "multipart/form-data");*/
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);


                con.setRequestProperty("Cache-Control", "no-cache");


                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+ base64);


                con.setDoOutput(true);
                  con.setDoInput(true);

                OutputStream outputStream = con.getOutputStream();

                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "utf-8"));


                writer.write(getPostDataString(postDataParams));
                Log.i("paramsData VINIIIII FCM", getPostDataString(postDataParams).toString());


                writer.flush();

                writer.close();
                outputStream.close();
                responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);
                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
               StringBuilder totaldummy = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet totaldummy ", totaldummy + "");

            } catch (Exception e) {

            }
            return responseCode + "";

        }

        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while (itr.hasNext()) {

                String key = itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            Log.i("Vineet Kumar G FCM", result.toString() + "");

            return result.toString();
        }

    }



    class UserBatches extends AsyncTask<Void,Void,String> {


        @Override
        protected String doInBackground(Void... voids) {

            try {


                URL obj = new URL(getString(R.string.server_link)+"Batch_Serializers_list");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");

                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                batchName.add(jsonObject2.optString("name").toString());

                    batchId.add(jsonObject2.optInt("id"));

                }

            } catch (Exception e) {

            }

            arrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, batchName);



            arrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


            uploadBatches.setAdapter(arrayAdapter);

        }
    }

class GetMainCategory extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL obj = new URL(getString(R.string.server_link)+"Main_Course_Category_list?format=json");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Notification Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                getMainCategoryBuilder = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    getMainCategoryBuilder.append(line).append('\n');
                }

                Log.i("Vineet Kumar Admin", getMainCategoryBuilder + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                mainCategoryNames.removeAll(mainCategoryNames);
                mainCategoryID.removeAll(mainCategoryID);
                JSONObject jsonObject = new JSONObject(getMainCategoryBuilder.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    mainCategoryNames.add(jsonObject2.optString("title").toString());

                    mainCategoryID.add((jsonObject2.optInt("id")));


                    Log.d("HELLO Main Category ",mainCategoryNames.toString());


                }


                mainCategoryArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, mainCategoryNames);



             //   mainCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                mainCategory.setAdapter(mainCategoryArrayAdapter);

            }catch (Exception e){

            }








        }

}




    class GetSubCategory extends AsyncTask<Integer,Void,Void> {

        int valueId;

        @Override
        protected Void doInBackground(Integer... voids) {

            try {

                valueId = voids[0];
                URL obj = new URL(getString(R.string.server_link)+"Super_Course_Category_Serializers_list/"+valueId);

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Notification Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                getSubCategoryBuilder = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    getSubCategoryBuilder.append(line).append('\n');
                }

                Log.i("Vineet Kumar Admin", getSubCategoryBuilder + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                subCategoryNames.removeAll(subCategoryNames);
                subCategoryID.removeAll(subCategoryID);



                JSONObject jsonObject = new JSONObject(getSubCategoryBuilder.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);



                    subCategoryNames.add(jsonObject2.optString("title").toString());

                   subCategoryID.add((jsonObject2.optInt("id")));


                    Log.d("HELLO Main Category ",mainCategoryNames.toString());


                }


                subCategoryArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, subCategoryNames);



            //    subCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                subcategory.setAdapter(subCategoryArrayAdapter);

            }catch (Exception e){

            }








        }

    }

    class GetInnerSubCategory extends AsyncTask<Integer,Void,Void> {

        int valueId;

        @Override
        protected Void doInBackground(Integer... voids) {

            try {

                valueId = voids[0];
                URL obj = new URL(getString(R.string.server_link)+"Sub_Course_Category_Serializers_list/"+valueId);

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Notification Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                getInnerSubCategoryBuilder = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    getInnerSubCategoryBuilder.append(line).append('\n');
                }

                Log.i("Vineet Kumar Admin", getInnerSubCategoryBuilder + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {

                innerSubCategoryNames.removeAll(innerSubCategoryNames);
                innerSubCategoryID.removeAll(innerSubCategoryID);


                JSONObject jsonObject = new JSONObject(getInnerSubCategoryBuilder.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    innerSubCategoryNames.add(jsonObject2.optString("title").toString());

                    innerSubCategoryID.add((jsonObject2.optInt("id")));


                    Log.d("HELLO Main Category ",innerSubCategoryNames.toString());


                }


                innerSubCategoryArrayAdapter = new ArrayAdapter(getActivity(), android.R.layout.simple_spinner_dropdown_item, innerSubCategoryNames);



               // innerSubCategoryArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);


                innerSubCategory.setAdapter(innerSubCategoryArrayAdapter);

            }catch (Exception e){

            }








        }

    }


}
