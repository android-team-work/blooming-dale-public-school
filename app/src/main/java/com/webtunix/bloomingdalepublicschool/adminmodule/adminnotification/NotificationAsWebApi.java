package com.webtunix.bloomingdalepublicschool.adminmodule.adminnotification;

import okhttp3.MultipartBody;
import retrofit2.Call;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface NotificationAsWebApi {


    String BASE_URL = "http://www.bloomingdalepublicschool.com/api/v4/";



    @Multipart
    @POST("Course_Content_Serializers_Save/")
    Call<MyResponseAsWeb> uploadddImage(@Part("main_course_category_id") Integer mainCouse, @Part("super_course_category_id") Integer superCourse,
                                  @Part("sub_course_category_id") Integer subCategory,@Part("title") String title,
                                  @Part MultipartBody.Part file,@Part("details") String details);

}


       /*  "pdf": null,
         "details": "",
         "trash": true,
         "date": "2020-02-01T17:28:41.372459+05:30",
         "video2": null,
         "main_course_category_id": 1,
         "super_course_category_id": 8,
         "sub_course_category_id": 8*/
