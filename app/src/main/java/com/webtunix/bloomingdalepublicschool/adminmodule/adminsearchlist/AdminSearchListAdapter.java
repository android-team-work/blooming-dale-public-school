package com.webtunix.bloomingdalepublicschool.adminmodule.adminsearchlist;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AutoCompleteTextView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


import com.webtunix.bloomingdalepublicschool.R;

import java.util.ArrayList;

public class AdminSearchListAdapter extends  RecyclerView.Adapter<AdminSearchListAdapter.ViewHolder>{


    Context  context;

    ArrayList<String> arrayList;

    ArrayList<Integer> spinnerId;

    ArrayList<String> phonenumber;
    AutoCompleteTextView autoCompleteTextView;

    public AdminSearchListAdapter(Context context, ArrayList<String> arrayList, ArrayList<Integer> spinnerId,
                                  ArrayList<String> phonenumber, AutoCompleteTextView autoCompleteTextView) {

        this.autoCompleteTextView = autoCompleteTextView;

this.context = context;

this.arrayList = arrayList;

this.spinnerId = spinnerId;

this.phonenumber = phonenumber;



    }
    @NonNull
    @Override
    public AdminSearchListAdapter.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.admin_searchlist_recycle_list, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
      ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull AdminSearchListAdapter.ViewHolder holder, int position) {



        holder.idview.setText((position+1)+"");
        holder.emailview.setText(arrayList.get(position));
        holder.phoneview.setText(phonenumber.get(position));


    }

    @Override
    public int getItemCount() {
        return arrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView idview,emailview,phoneview;

        LinearLayout linearLayout;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);


            idview = itemView.findViewById(R.id.searchlistIDtextView);
            emailview = itemView.findViewById(R.id.searchlistEmailTextView);
            phoneview = itemView.findViewById(R.id.searchlistMobileTextView);

linearLayout = itemView.findViewById(R.id.linearlayoutadminsearchRecycler);

       linearLayout.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View view) {


               int itemPosition = getAdapterPosition();

               Bundle bundleString = new Bundle();
               bundleString.putString("selectuser",""+spinnerId.get(itemPosition));
               Navigation.findNavController(view).navigate(R.id.adminUserDeatilFragment,bundleString);


           }
       });



        }


        @Override
        public void onClick(View view) {

        }
    }
}
