package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.specialcontent;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.webtunix.bloomingdalepublicschool.R;

import java.util.ArrayList;


public class SpecialContentAdapter  extends  RecyclerView.Adapter<SpecialContentAdapter.ViewHolder>  {



    ArrayList<String> titlee = new ArrayList<String>();
    ArrayList<Integer> spinnerid = new ArrayList<Integer>();
    ArrayList<String> description = new ArrayList<String>();
    ArrayList<String> imageurl = new ArrayList<String>();




    Context context;

    public SpecialContentAdapter( Context context,ArrayList<String> title, ArrayList<Integer> spinnerid,ArrayList<String> imageurl) {




        this.titlee = title;
        this.context = context;
        this.spinnerid = spinnerid;
        this.imageurl = imageurl;


    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.specialcontentlistrecycler, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
       ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        Glide.with(context)
                .load(imageurl.get(position))
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .into(holder.imageView);







        holder.textView.setText(titlee.get(position));



    }

    @Override
    public int getItemCount() {
        return titlee.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView textView;
        ImageView imageView;

        CardView cardView;

        public ViewHolder(@NonNull final View itemView) {
            super(itemView);

            imageView = itemView.findViewById(R.id.mainscreencategoryImage);

            textView = itemView.findViewById(R.id.mainCategoryTitle);

            cardView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    int itemPosition = getAdapterPosition();



                    Bundle bundleString = new Bundle();
                    Bundle bundle1 = new Bundle();
                    bundle1.putString("SubCAtegoryID",""+spinnerid.get(itemPosition));
                    bundle1.putString("LabelHomeCategory",titlee.get(itemPosition));

                    Navigation.findNavController(view).getGraph().findNode(R.id.specialContentSinglePage).setLabel(titlee.get(itemPosition));

                    Navigation.findNavController(view).navigate(R.id.specialContentSinglePage,bundleString);




                }
            });




        }

        public void onClick(View view) {

        }






    }
}
