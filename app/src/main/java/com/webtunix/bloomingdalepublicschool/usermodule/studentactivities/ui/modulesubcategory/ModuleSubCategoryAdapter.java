package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.modulesubcategory;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.navigation.Navigation;
import androidx.recyclerview.widget.RecyclerView;


import com.webtunix.bloomingdalepublicschool.R;

import java.util.ArrayList;

public class ModuleSubCategoryAdapter extends  RecyclerView.Adapter<ModuleSubCategoryAdapter.ViewHolder> {

        ArrayList<String> arrayList = new ArrayList<String>();
        ArrayList<String> imageurl = new ArrayList<String>();
        ArrayList<Integer> spinnerid = new ArrayList<Integer>();



        Context context;


public ModuleSubCategoryAdapter( Context context,ArrayList<String> arrayList, ArrayList<Integer> spinnerid) {




        this.arrayList = arrayList;
        this.context = context;
        this.spinnerid = spinnerid;

        }



@NonNull
@Override
public ViewHolder onCreateViewHolder( ViewGroup parent, int viewType) {


        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.module_subcategory_layout, parent, false);
        view.setLayoutParams(new RecyclerView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,ViewGroup.LayoutParams.WRAP_CONTENT));
        ViewHolder myViewHolder = new ViewHolder(view);
        return myViewHolder;

        }

@Override
public void onBindViewHolder(@NonNull ViewHolder holder, int position) {




           holder.textView.setText(arrayList.get(position));


    Log.d("Module Subcategory ","I am here only");





        }

@Override
public int getItemCount() {
        return arrayList.size();
        }

public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    TextView textView;

    public ViewHolder(@NonNull final View itemView) {
        super(itemView);


         textView = itemView.findViewById(R.id.modulesubcategorytextview);

         textView.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View view) {

                 int itemPosition = getAdapterPosition();



                 Bundle bundleString = new Bundle();
                 bundleString.putString("ModuleSubCategoryPosition",""+spinnerid.get(itemPosition));

                 Navigation.findNavController(view).getGraph().findNode(R.id.InnerModuleSubcategoryFragment).setLabel(arrayList.get(itemPosition));

                 Navigation.findNavController(view).navigate(R.id.InnerModuleSubcategoryFragment,bundleString);




             }
         });




    }
    @Override
    public void onClick(View view) {

    }



}
}
