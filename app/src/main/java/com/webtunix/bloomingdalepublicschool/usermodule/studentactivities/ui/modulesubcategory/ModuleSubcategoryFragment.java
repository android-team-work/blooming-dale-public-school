package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.modulesubcategory;

import android.graphics.Bitmap;
import android.graphics.Rect;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;


import com.webtunix.bloomingdalepublicschool.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ModuleSubcategoryFragment extends Fragment {

    private ModuleSubCategoryAdapter moduleSubCategoryAdapter;



    ArrayList<Bitmap> bitmaps;
    RecyclerView recyclerView;

    ArrayList<String> arrayList ;
    ArrayList<Integer> spinnerId ;

    ArrayList<String> imageurl ;

    StringBuilder total;
TextView emptymodulesubcategory;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        // Inflate the layout for this fragment
        View root = inflater.inflate(R.layout.fragment_module_subcategory, container, false);





        recyclerView  = root.findViewById(R.id.innersubcategorymoduleID);
        arrayList = new ArrayList<String>();
        imageurl = new ArrayList<String>();
        bitmaps = new ArrayList<Bitmap>();

        emptymodulesubcategory = root.findViewById(R.id.emptyModulesubcategory);

        spinnerId = new ArrayList<Integer>();

        DoSomething doSomething = new DoSomething();

        doSomething.execute();
        Log.d("Chaleya Ki ",arrayList.toString());

        return root;



    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }

    class DoSomething extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {


                Log.d("Getting Values : ",""+getArguments().getString("POSITION"));
                URL obj = new URL(getString(R.string.server_link)+"Sub_Course_Category_Serializers_list/"+getArguments().getString("POSITION"));

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    arrayList.add(jsonObject2.optString("title").toString());

                    spinnerId.add((jsonObject2.optInt("id")));


                    Log.d("HELLO ARRAYLIST AAYI ",arrayList.toString());
                    Log.d("HELLO ARRAYLIST AAYI ",imageurl.toString());


                }

            }catch (Exception e){

            }
            if(arrayList.isEmpty()&&spinnerId.isEmpty()){

                emptymodulesubcategory.setVisibility(View.VISIBLE);
            }else{
                emptymodulesubcategory.setVisibility(View.GONE);

            }

            moduleSubCategoryAdapter = new ModuleSubCategoryAdapter(getContext(),arrayList,spinnerId);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(moduleSubCategoryAdapter);
             recyclerView.addItemDecoration(new VerticalSpaceItemDecoration(1));





        }
    }
    public class VerticalSpaceItemDecoration extends RecyclerView.ItemDecoration {

        private final int verticalSpaceHeight;

        public VerticalSpaceItemDecoration(int verticalSpaceHeight) {
            this.verticalSpaceHeight = verticalSpaceHeight;
        }

        @Override
        public void getItemOffsets(Rect outRect, View view, RecyclerView parent,
                                   RecyclerView.State state) {
            outRect.bottom = verticalSpaceHeight;
            if (parent.getChildAdapterPosition(view) != parent.getAdapter().getItemCount() - 1) {
                outRect.bottom = verticalSpaceHeight;
            }
        }
    }
}
