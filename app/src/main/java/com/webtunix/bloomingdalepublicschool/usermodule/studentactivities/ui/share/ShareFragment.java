package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.share;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.webtunix.bloomingdalepublicschool.prelogin.SignIn;


public class ShareFragment extends Fragment {

    private ShareViewModel shareViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
      /*  shareViewModel =
                ViewModelProviders.of(this).get(ShareViewModel.class);
        View root = inflater.inflate(R.layout.fragment_share, container, false);
        final TextView textView = root.findViewById(R.id.text_share);
        shareViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;*/

        SharedPreferences sharedPreferences = getActivity().getSharedPreferences("techapppref", Context.MODE_PRIVATE);

        SharedPreferences.Editor myEdit = sharedPreferences.edit();


        myEdit.putString("useremail","nul");
        myEdit.putString("userpassword","nul");

        myEdit.commit();


        Intent  intent = new Intent(getContext(), SignIn.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
        Toast.makeText(getContext(),"You are successfully Logged Out, Thank You",Toast.LENGTH_SHORT).show();

        getActivity().finish();
      return null;
    }


}