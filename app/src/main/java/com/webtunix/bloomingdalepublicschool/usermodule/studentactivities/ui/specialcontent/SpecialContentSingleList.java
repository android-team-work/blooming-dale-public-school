package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.specialcontent;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.webtunix.bloomingdalepublicschool.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class SpecialContentSingleList extends Fragment {

    Button pdfHomeSetting;
    EditText pdfgoEdittext;
    Button toggleButton;
    View splitLine;


    ArrayList<String> arrayList;
    ArrayList<Integer> spinnerId;

    ArrayList<String> imageurl;
    ArrayList<String> videolist;
    ArrayList<String> descriptionList;

    ArrayList<String> pdflist;


    StringBuilder total;


    Context context;

    PDFView pdfView;
    ImageView imageView;

    TextView detailView, titletextview;


    View view = null;
    YouTubePlayerFragment youtubeFragment = null;

    VideoView videoView;

    LinearLayout linearLayout;

    FrameLayout frameLayout;
    TextView mainHeading;
    String pdfMode;

    boolean pdfmodebool = false;
// YouTUbe Player Fargment

    private YouTubePlayer YPlayer;
    private static final String YoutubeDeveloperKey = "AIzaSyAqVTpWcJ9a6-HjsTcNyT_oAmLEYFArJbU";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";

//audio Controls

    LinearLayout audioLinearLayout;

    TextView audioHeading, audioTimeshow;

    SeekBar audioSeekBar;

    Button floatingActionButtonAudio;

    MediaPlayer mediaPlayer;
    boolean wasPlaying = false;
    String userName, userPassword, userStatus, autherString;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        SharedPreferences sh = getActivity().getSharedPreferences("techapppPDF", Context.MODE_PRIVATE);

        SharedPreferences sh1 = getActivity().getSharedPreferences("techapppref", Context.MODE_PRIVATE);

        userName = sh1.getString("useremail", "");
        userPassword = sh1.getString("userpassword", "");
        userStatus =sh1.getString("userType", "");

        autherString = userName.trim()+":"+userPassword.trim();


        pdfMode = sh.getString("pdfMode", "");

        if (pdfMode.equals("OFF")) {
            pdfmodebool = false;


        } else if (pdfMode.equals("ON")) {
            pdfmodebool = true;
        }

        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.setMicrophoneMute(true);
        }

        youtubeFragment = null;

        view = inflater.inflate(R.layout.fragment_special_content_single_list, container, false);

        SharedPreferences sho = getActivity().getSharedPreferences("techapppref", Context.MODE_PRIVATE);

        userName = sho.getString("useremail", "");
        userPassword = sho.getString("userpassword", "");
        userStatus = sho.getString("userType", "");

        autherString = userName.trim() + ":" + userPassword.trim();

        //audio controls

        audioLinearLayout = view.findViewById(R.id.specialContentaudioLinearLayout);
        audioHeading = view.findViewById(R.id.specialContentaudioHeading);
        audioTimeshow = view.findViewById(R.id.specialContentaudioTimeShow);
        audioSeekBar = view.findViewById(R.id.specialContentaudioseekbar);
        floatingActionButtonAudio = view.findViewById(R.id.specialContentauidoPlayPause);

        mediaPlayer = new MediaPlayer();

        pdfHomeSetting = view.findViewById(R.id.specialContentpdfHomeSetting);
        toggleButton = view.findViewById(R.id.specialContentpdfSwipeEnable);
        pdfgoEdittext = view.findViewById(R.id.specialContentpdfgotosetting);
        splitLine = view.findViewById(R.id.specialContentSplitLine_hor1);
        videoView = view.findViewById(R.id.specialContentvideoplayer);


        pdfHomeSetting.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                pdfView.jumpTo(0);
            }
        });

        toggleButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {


                pdfView.jumpTo(Integer.parseInt(pdfgoEdittext.getText().toString()));
            }
        });


        arrayList = new ArrayList<String>();
        imageurl = new ArrayList<String>();
        videolist = new ArrayList<String>();
        pdflist = new ArrayList<String>();
        spinnerId = new ArrayList<Integer>();
        descriptionList = new ArrayList<String>();


        linearLayout = view.findViewById(R.id.specialContentVideoLinearLayout);
        frameLayout = view.findViewById(R.id.specialContentpdfFrame);
        pdfView = view.findViewById(R.id.specialContentpdfView);
        /*   imageView = view.findViewById(R.id.innerdescrtionImageview);*/
        //  textView = view.findViewByIdToggleButton(R.id.innerdescriptionTextView);
        titletextview = view.findViewById(R.id.specialContentsinglecontentTitleTextView);

        mainHeading = view.findViewById(R.id.specialContentSingleContentMainTitleTextView);

        detailView = view.findViewById(R.id.specialContentsinglecontentDetailTextView);
        imageView = view.findViewById(R.id.specialContentcontentDeatilImageView);
     /*  youtubeFragment = (YouTubePlayerFragment)
         getActivity().getFragmentManager().findFragmentById(R.id.youtubeFragment);*/

        if (userStatus.equals("0")) {

            //Student Notification
            new StudentDoSomething().execute(autherString);


        } else {

            //Admin Notification

            new StudentDoSomething().execute(autherString);
        }

/*DoSomething doSomething = new DoSomething();

        doSomething.execute();*/
        Log.d("Chaleya Ki Module wala", arrayList.toString());


        floatingActionButtonAudio.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                playSong();
            }
        });

        audioSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean b) {

                audioTimeshow.setVisibility(View.VISIBLE);
                int x = (int) Math.ceil(progress / 1000f);

                if (x < 10)
                    audioTimeshow.setText("0:0" + x);
                else
                    audioTimeshow.setText("0:" + x);

                double percent = progress / (double) seekBar.getMax();
                int offset = seekBar.getThumbOffset();
                int seekWidth = seekBar.getWidth();
                int val = (int) Math.round(percent * (seekWidth - 2 * offset));
                int labelWidth = audioTimeshow.getWidth();
                audioTimeshow.setX(offset + seekBar.getX() + val
                        - Math.round(percent * offset)
                        - Math.round(percent * labelWidth / 2));

                if (progress > 0 && mediaPlayer != null && !mediaPlayer.isPlaying()) {
                    clearMediaPlayer();
                    floatingActionButtonAudio.setBackgroundResource(android.R.drawable.ic_media_play);
                    seekBar.setProgress(0);
                }

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

                audioTimeshow.setVisibility(View.VISIBLE);


            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {


                if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                    mediaPlayer.seekTo(seekBar.getProgress());
                }
            }


        });


        return view;
    }

    @Override
    public void onPause() {
        super.onPause();
        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager != null) {
            audioManager.setMicrophoneMute(false);
        }

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        clearMediaPlayer();
   /*     youtubeFragment  = (YouTubePlayerFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.youtubeFragment);
        if (youtubeFragment != null)
            getActivity().getFragmentManager().beginTransaction().remove(youtubeFragment).commit();*/
    }

    class DoSomething extends AsyncTask<Void, Void, Void> {

        InputStream io;

        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL obj = new URL(getString(R.string.server_link) + "Course_Content_Serializers_Private/" + getArguments().getString("SpecialContentSingleID"));


                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int responseCode = con.getResponseCode();


                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");


            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {

                String arrayListValue;
                String descriptionListValue;
                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    arrayListValue = jsonObject2.optString("title").toString();
                    arrayListValue = arrayListValue.replace("\"", "");

                    arrayList.add(arrayListValue);
                    imageurl.add(getString(R.string.pureServer_link) + jsonObject2.optString("image").toString());

                    descriptionListValue = jsonObject2.optString("details").toString();
                    descriptionListValue = descriptionListValue.replace("\"", "");


                    descriptionList.add(descriptionListValue);
                    spinnerId.add((jsonObject2.optInt("id")));


                    Log.d("HELLO ARRAYLIST AAYI ", arrayList.toString());
                    Log.d("HELLO ARRAYLIST AAYI ", videolist.toString());


                }

            } catch (Exception e) {

            }


            if (!(arrayList.get(0).equals("http://ec2-52-15-107-53.us-east-2.compute.amazonaws.comnull"))) {

                mainHeading.setText(arrayList.get(0).toString());


            } else {

                titletextview.setText("Course Contents");
                Log.d("SOMETHING SOMETHING", pdflist.get(0) + " Kuch hai");
            }

            if ((imageurl.get(0).contains(".mp3")) || (imageurl.get(0).contains(".mp4"))) {


                linearLayout.setVisibility(View.VISIBLE);
                videoView.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);


                Log.d("Video  WALA ELSE", "Video  IS NOT  EMPTY");
                Log.d("Video  WALA ELSE", "h" + videolist.get(0) + "V");
                Log.d("PDF URL", pdflist.get(0).toString());

                String url = videolist.get(0);
                if (url.contains(".mp3")) {
                    audioLinearLayout.setVisibility(View.VISIBLE);

                    videoView.setVisibility(View.GONE);


                    imageView.setVisibility(View.GONE);
                    pdfHomeSetting.setVisibility(View.GONE);
                    toggleButton.setVisibility(View.GONE);
                    pdfgoEdittext.setVisibility(View.GONE);
                    splitLine.setVisibility(View.GONE);
                    titletextview.setText(arrayList.get(0).toString());
                    detailView.setText(descriptionList.get(0).toString());

                    Log.d("MP3", "MP3");
                } else {
                    Log.d("MP4", "MP4");

                    MediaController mediaController = new MediaController(getActivity());
                    mediaController.setAnchorView(videoView);

                    Uri uri = Uri.parse(videolist.get(0).toString());

                    videoView.setMediaController(mediaController);
                    videoView.setVideoURI(uri);
                    videoView.requestFocus();
                    videoView.start();
                    final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "Please wait ...", "Retrieving data ...", true);

                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                        public void onPrepared(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            progDailog.dismiss();
                        }
                    });
                    imageView.setVisibility(View.GONE);
                    pdfHomeSetting.setVisibility(View.GONE);
                    toggleButton.setVisibility(View.GONE);
                    pdfgoEdittext.setVisibility(View.GONE);
                    splitLine.setVisibility(View.GONE);
                    titletextview.setText(arrayList.get(0).toString());
                    detailView.setText(descriptionList.get(0).toString());


                }
            } else if (imageurl.get(0).contains(".pdf")) {
                frameLayout.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.GONE);
                pdfHomeSetting.setVisibility(View.VISIBLE);
                toggleButton.setVisibility(View.VISIBLE);
                pdfgoEdittext.setVisibility(View.VISIBLE);
                splitLine.setVisibility(View.VISIBLE);

                Log.d("PDF MODE ", pdfmodebool + "");

                new DrawPDF().execute();

                Log.d("PDF URL", pdflist.get(0).toString());


            } else {
                linearLayout.setVisibility(View.VISIBLE);
                videoView.setVisibility(View.GONE);
                frameLayout.setVisibility(View.GONE);


                imageView.setVisibility(View.VISIBLE);
                Glide.with(getContext())
                        .load(imageurl.get(0))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
                pdfHomeSetting.setVisibility(View.GONE);
                toggleButton.setVisibility(View.GONE);
                pdfgoEdittext.setVisibility(View.GONE);
                splitLine.setVisibility(View.GONE);
                titletextview.setText(arrayList.get(0).toString());
                detailView.setText(descriptionList.get(0).toString());
            }
        }

        String getVideoId(String videoUrl) {
            if (videoUrl == null || videoUrl.trim().length() <= 0)
                return null;

            Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(videoUrl);

            if (matcher.find())
                return matcher.group(1);
            return null;
        }
    }

    class DrawPDF extends AsyncTask<Void, Void, Void> {

        InputStream io;

        @Override
        protected Void doInBackground(Void... voids) {
            try {
                io = new URL(pdflist.get(0).toString()).openStream();

            } catch (Exception e) {

            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);


            try {


                pdfView.fromStream(io)

                        .enableSwipe(true) // allows to block changing pages using swipe
                        .swipeHorizontal(false)
                        .enableDoubletap(true)
                        .pageFling(true)
                        .nightMode(pdfmodebool)
                        .fitEachPage(true)

                        .load();
            } catch (Exception e) {

            }


        }
    }

    public void playSong() {

        try {


            if (mediaPlayer != null && mediaPlayer.isPlaying()) {
                clearMediaPlayer();
                audioSeekBar.setProgress(0);
                wasPlaying = true;
                floatingActionButtonAudio.setBackgroundResource(android.R.drawable.ic_media_play);
            }


            if (!wasPlaying) {

                if (mediaPlayer == null) {
                    mediaPlayer = new MediaPlayer();
                }
                floatingActionButtonAudio.setBackgroundResource(android.R.drawable.ic_media_pause);
                Uri uri = Uri.parse(videolist.get(0).toString());


                /*Uri mp3 = Uri.parse("android.resource://"

                        + getActivity().getPackageName() + "/raw/"
                        + "sample");*/

                mediaPlayer = new MediaPlayer();

                mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

                mediaPlayer.setDataSource(getContext(), uri);

                mediaPlayer.prepare();

                mediaPlayer.setVolume(0.5f, 0.5f);
                mediaPlayer.setLooping(false);
                audioSeekBar.setMax(mediaPlayer.getDuration());

                mediaPlayer.start();

                new Thread().start();


            }

            wasPlaying = false;
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public void run() {

        int currentPosition = mediaPlayer.getCurrentPosition();
        int total = mediaPlayer.getDuration();


        while (mediaPlayer != null && mediaPlayer.isPlaying() && currentPosition < total) {
            try {
                Thread.sleep(1000);
                currentPosition = mediaPlayer.getCurrentPosition();
            } catch (InterruptedException e) {
                return;
            } catch (Exception e) {
                return;
            }

            audioSeekBar.setProgress(currentPosition);

        }
    }


    private void clearMediaPlayer() {
        mediaPlayer.stop();
        mediaPlayer.release();
        mediaPlayer = null;
    }


    class StudentDoSomething extends AsyncTask<String, Void, Void> {

        InputStream io;

        @Override
        protected Void doInBackground(String... strings) {

            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                URL obj = new URL(getString(R.string.server_link) + "Course_Content_Serializers_Private/" + getArguments().getString("SpecialContentSingleID"));


                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);

                int responseCode = con.getResponseCode();


                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");


            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                String arrayListValue;
                String descriptionListValue;

                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                    arrayListValue = jsonObject2.optString("title").toString();
                    arrayListValue = arrayListValue.replace("\"", "");

                    arrayList.add(arrayListValue);


                    imageurl.add(getString(R.string.pureServer_link) + jsonObject2.optString("image").toString());

                    descriptionListValue = jsonObject2.optString("details").toString();
                    descriptionListValue = descriptionListValue.replace("\"", "");


                    descriptionList.add(descriptionListValue);

                    spinnerId.add((jsonObject2.optInt("id")));


                    Log.d("HELLO ARRAYLIST AAYI ", arrayListValue.toString());
                    Log.d("HELLO descriptin AAYI ", descriptionListValue.toString());


                }

            } catch (Exception e) {

            }


            if (!(arrayList.get(0).equals("http://ec2-52-15-107-53.us-east-2.compute.amazonaws.comnull"))) {

                mainHeading.setText(arrayList.get(0).toString());


            } else {

                titletextview.setText("Course Contents");
                Log.d("SOMETHING SOMETHING", pdflist.get(0) + " Kuch hai");
            }

            if ((imageurl.get(0).contains(".mp3")) || (imageurl.get(0).contains(".mp4"))) {


                linearLayout.setVisibility(View.VISIBLE);
                videoView.setVisibility(View.VISIBLE);
                frameLayout.setVisibility(View.GONE);


                Log.d("Video  WALA ELSE", "Video  IS NOT  EMPTY");
                Log.d("Video  WALA ELSE", "h" + videolist.get(0) + "V");
                Log.d("PDF URL", pdflist.get(0).toString());

                String url = videolist.get(0);
                if (url.contains(".mp3")) {
                    audioLinearLayout.setVisibility(View.VISIBLE);

                    videoView.setVisibility(View.GONE);


                    imageView.setVisibility(View.GONE);
                    pdfHomeSetting.setVisibility(View.GONE);
                    toggleButton.setVisibility(View.GONE);
                    pdfgoEdittext.setVisibility(View.GONE);
                    splitLine.setVisibility(View.GONE);
                    titletextview.setText(arrayList.get(0).toString());
                    detailView.setText(descriptionList.get(0).toString());

                    Log.d("MP3", "MP3");
                } else {
                    Log.d("MP4", "MP4");

                    MediaController mediaController = new MediaController(getActivity());
                    mediaController.setAnchorView(videoView);

                    Uri uri = Uri.parse(videolist.get(0).toString());

                    videoView.setMediaController(mediaController);
                    videoView.setVideoURI(uri);
                    videoView.requestFocus();
                    videoView.start();
                    final ProgressDialog progDailog = ProgressDialog.show(getActivity(), "Please wait ...", "Retrieving data ...", true);

                    videoView.setOnPreparedListener(new MediaPlayer.OnPreparedListener() {

                        public void onPrepared(MediaPlayer mp) {
                            // TODO Auto-generated method stub
                            progDailog.dismiss();
                        }
                    });
                    imageView.setVisibility(View.GONE);
                    pdfHomeSetting.setVisibility(View.GONE);
                    toggleButton.setVisibility(View.GONE);
                    pdfgoEdittext.setVisibility(View.GONE);
                    splitLine.setVisibility(View.GONE);
                    titletextview.setText(arrayList.get(0).toString());
                    detailView.setText(descriptionList.get(0).toString());


                }
            } else if (imageurl.get(0).contains(".pdf")) {
                frameLayout.setVisibility(View.VISIBLE);
                linearLayout.setVisibility(View.GONE);
                pdfHomeSetting.setVisibility(View.VISIBLE);
                toggleButton.setVisibility(View.VISIBLE);
                pdfgoEdittext.setVisibility(View.VISIBLE);
                splitLine.setVisibility(View.VISIBLE);

                Log.d("PDF MODE ", pdfmodebool + "");

                new DrawPDF().execute();

                Log.d("PDF URL", pdflist.get(0).toString());


            } else {
                linearLayout.setVisibility(View.VISIBLE);
                videoView.setVisibility(View.GONE);
                frameLayout.setVisibility(View.GONE);


                imageView.setVisibility(View.VISIBLE);
                Glide.with(getContext())
                        .load(imageurl.get(0))
                        .diskCacheStrategy(DiskCacheStrategy.ALL)
                        .into(imageView);
                pdfHomeSetting.setVisibility(View.GONE);
                toggleButton.setVisibility(View.GONE);
                pdfgoEdittext.setVisibility(View.GONE);
                splitLine.setVisibility(View.GONE);
                titletextview.setText(arrayList.get(0).toString());
                detailView.setText(descriptionList.get(0).toString());
            }
        }

        String getVideoId(String videoUrl) {
            if (videoUrl == null || videoUrl.trim().length() <= 0)
                return null;

            Pattern pattern = Pattern.compile(reg, Pattern.CASE_INSENSITIVE);
            Matcher matcher = pattern.matcher(videoUrl);

            if (matcher.find())
                return matcher.group(1);
            return null;
        }
    }
}


