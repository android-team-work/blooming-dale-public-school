package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.gallery;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;


import com.webtunix.bloomingdalepublicschool.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GalleryFragment extends Fragment {


    NotificationAdapter  notificationAdapter;

    TextView emptyNotification;

    StringBuilder total;

    ArrayList<String> title;

    ArrayList<String> image;

    ArrayList<String> description;

    ArrayList<Integer> spinnerId;

    RecyclerView recyclerView;

    View root;
String userName,userPassword,userStatus,autherString;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);

        SharedPreferences sh = getActivity().getSharedPreferences("techapppref", Context.MODE_PRIVATE);

        userName = sh.getString("useremail", "");
        userPassword = sh.getString("userpassword", "");
        userStatus =sh.getString("userType", "");

        autherString = userName.trim()+":"+userPassword.trim();


        root = inflater.inflate(R.layout.fragment_gallery, container, false);
emptyNotification = root.findViewById(R.id.emptyNotification);

         title = new ArrayList<>();

         image = new ArrayList<>();

         description = new ArrayList<>();

         spinnerId = new ArrayList<>();


         recyclerView = root.findViewById(R.id.notificationListRecycler);

         if(userStatus.equals("0")){

             //Student Notification

             new StudentDoSomething().execute(autherString);

         }else{

             //Admin Notification

             new DoSomething().execute();
         }




        return root;
    }


    class DoSomething extends AsyncTask<Void,Void,Void> {


        @Override
        protected Void doInBackground(Void... voids) {

            try {



                URL obj = new URL(getString(R.string.server_link)+"Student_Notification_Serializers_list");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {

                String arrayListValue;
                String descriptionListValue;
                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = (jsonArray.length()-1); i >= 0; i--) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                    arrayListValue = jsonObject2.optString("title").toString();
                    arrayListValue= arrayListValue.replace("\"", "");

                    title.add(arrayListValue);
                    image.add(getString(R.string.pureServer_link)+jsonObject2.optString("image").toString());

                    descriptionListValue = jsonObject2.optString("details").toString();
                    descriptionListValue= descriptionListValue.replace("\"", "");


                    description.add(descriptionListValue);

                    spinnerId.add((jsonObject2.optInt("id")));




                }

            }catch (Exception e){

            }

            if(title.isEmpty()&&image.isEmpty()&& description.isEmpty()){

                emptyNotification.setVisibility(View.VISIBLE);
            }else{
                emptyNotification.setVisibility(View.GONE);

            }
            notificationAdapter = new NotificationAdapter(getContext(),title,spinnerId,description,image);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(notificationAdapter);





        }
    }
    class StudentDoSomething extends AsyncTask<String,Void,Void> {


        @Override
        protected Void doInBackground(String... strings) {

            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                URL obj = new URL(getString(R.string.server_link)+"Student_Notification_list");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);

                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {

                String arrayListValue;
                String descriptionListValue;
                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = (jsonArray.length()-1); i >= 0; i--) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);

                    arrayListValue = jsonObject2.optString("title").toString();
                    arrayListValue= arrayListValue.replace("\"", "");

                    title.add(arrayListValue);
                    image.add(getString(R.string.pureServer_link)+jsonObject2.optString("image").toString());
                    descriptionListValue = jsonObject2.optString("details").toString();
                    descriptionListValue= descriptionListValue.replace("\"", "");


                    description.add(descriptionListValue);                    spinnerId.add((jsonObject2.optInt("id")));




                }

            }catch (Exception e){

            }

            if(title.isEmpty()&&image.isEmpty()&& description.isEmpty()){

                emptyNotification.setVisibility(View.VISIBLE);
            }else{
                emptyNotification.setVisibility(View.GONE);

            }

            Log.d("Array values ",""+title+"   "+image+"  "+description);
            Log.d("Notification Bhai ",""+title+"   "+image+"  "+description);


            notificationAdapter = new NotificationAdapter(getContext(),title,spinnerId,description,image);

            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setAdapter(notificationAdapter);





        }
    }
}