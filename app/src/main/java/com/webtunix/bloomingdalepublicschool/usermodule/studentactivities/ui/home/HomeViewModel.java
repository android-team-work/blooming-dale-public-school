package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.home;

import android.widget.ImageView;
import android.widget.TextView;

public class HomeViewModel {


ImageView imageView;

TextView textView;


public HomeViewModel(){


}

    public ImageView getImageView() {
        return imageView;
    }

    public void setImageView(ImageView imageView) {
        this.imageView = imageView;
    }

    public TextView getTextView() {
        return textView;
    }

    public void setTextView(TextView textView) {
        this.textView = textView;
    }
}