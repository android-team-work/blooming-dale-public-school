package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.liveclass;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.VideoView;

import com.github.barteksc.pdfviewer.PDFView;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerFragment;
import com.webtunix.bloomingdalepublicschool.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class LiveClassSingleFragment extends Fragment  {
    Button pdfHomeSetting;
    EditText pdfgoEdittext;
    Button toggleButton;
    View splitLine;



    ArrayList<String> arrayList;
    ArrayList<Integer> spinnerId;

    ArrayList<String> imageurl;
    ArrayList<String> videolist;
    ArrayList<String> descriptionList;

    ArrayList<String> pdflist;


    StringBuilder total;


    Context context;

    PDFView pdfView;
    ImageView imageView;

    TextView detailView, titletextview;


    View view=null;
    YouTubePlayerFragment youtubeFragment=null;

    VideoView videoView;

    LinearLayout linearLayout;

    FrameLayout frameLayout;
    TextView mainHeading;
    String pdfMode;

    boolean pdfmodebool=false;
// YouTUbe Player Fargment

    private YouTubePlayer YPlayer;
    private static final String YoutubeDeveloperKey = "AIzaSyAqVTpWcJ9a6-HjsTcNyT_oAmLEYFArJbU";
    private static final int RECOVERY_DIALOG_REQUEST = 1;

    String reg = "(?:youtube(?:-nocookie)?\\.com\\/(?:[^\\/\\n\\s]+\\/\\S+\\/|(?:v|e(?:mbed)?)\\/|\\S*?[?&]v=)|youtu\\.be\\/)([a-zA-Z0-9_-]{11})";

//audio Controls

    LinearLayout audioLinearLayout;

    TextView audioHeading, audioTimeshow;

    SeekBar audioSeekBar;

    Button floatingActionButtonAudio;
    ProgressDialog mProgressDialog;
    LinearLayout bottomNavigationView,upperViewLinearLayout;
    MediaPlayer mediaPlayer;
    boolean wasPlaying = false;
    String userName,userPassword,userStatus,autherString;

    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        SharedPreferences sh = getActivity().getSharedPreferences("techapppPDF",Context.MODE_PRIVATE);
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);

        pdfMode = sh.getString("pdfMode","");

        if(pdfMode.equals("OFF")){
            pdfmodebool=false;


        }else if(pdfMode.equals("ON")){
            pdfmodebool = true;
        }

        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager!=null) {
            audioManager.setMicrophoneMute(true);
        }

        youtubeFragment = null;

        view   = inflater.inflate(R.layout.fragment_live_class_single, container, false);

       /* LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(100, 0);
        lp.weight = 2;
        upperViewLinearLayout.setLayoutParams(lp);*/
        bottomNavigationView = (LinearLayout) getActivity().findViewById(R.id.bottomViewRealtiveLayout);
        bottomNavigationView.setVisibility(View.GONE);
        SharedPreferences sho = getActivity().getSharedPreferences("techapppref", Context.MODE_PRIVATE);

        userName = sho.getString("useremail", "");
        userPassword = sho.getString("userpassword", "");
        userStatus =sho.getString("userType", "");

        autherString = userName.trim()+":"+userPassword.trim();

        //audio controls


        mediaPlayer = new MediaPlayer();






        mProgressDialog = new ProgressDialog(getActivity());
        mProgressDialog.setMessage("Loading PDF... \n Please wait ");
        mProgressDialog.setIndeterminate(true);




        arrayList = new ArrayList<String>();
        imageurl = new ArrayList<String>();
        videolist = new ArrayList<String>();
        pdflist = new ArrayList<String>();
        spinnerId = new ArrayList<Integer>();
        descriptionList = new ArrayList<String>();





       youtubeFragment = (YouTubePlayerFragment)
         getActivity().getFragmentManager().findFragmentById(R.id.youtubeFragment);

            //Student Notification
            new StudentDoSomething().execute();




/*DoSomething doSomething = new DoSomething();

        doSomething.execute();*/
        Log.d("Chaleya Ki Module wala", arrayList.toString());







        return view;
    }

    @Override
    public void onPause() {
        super.onPause();

        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager!=null) {
            audioManager.setMicrophoneMute(false);
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager!=null) {
            audioManager.setMicrophoneMute(true);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);


        AudioManager audioManager =
                (AudioManager) getActivity().getSystemService(Context.AUDIO_SERVICE);
        if (audioManager!=null) {
            audioManager.setMicrophoneMute(true);
        }
    }
    @SuppressLint("SourceLockedOrientationActivity")
    @Override
    public void onDestroyView() {
        super.onDestroyView();
      youtubeFragment  = (YouTubePlayerFragment) getActivity().getFragmentManager()
                .findFragmentById(R.id.youtubeFragment);
        if (youtubeFragment != null)
            getActivity().getFragmentManager().beginTransaction().remove(youtubeFragment).commit();
        getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        bottomNavigationView.setVisibility(View.VISIBLE);

    }











    class StudentDoSomething extends AsyncTask<Void, Void, Void> {

        InputStream io;
        @Override
        protected Void doInBackground(Void... voids) {

            try {
                URL obj = new URL(getString(R.string.server_link)+"Live_single/"+getArguments().getString("liveclassSingleID"));


                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                int responseCode = con.getResponseCode();


                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                total = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    total.append(line).append('\n');
                }

                Log.i("Vineet Kumar ", total + "");



            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @SuppressLint("SourceLockedOrientationActivity")
        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {
                String arrayListValue;
                String descriptionListValue;

                JSONObject jsonObject = new JSONObject(total.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);


                    descriptionListValue = jsonObject2.optString("details").toString();
                    descriptionListValue = descriptionListValue.replace("\"", "");


                    descriptionList.add(descriptionListValue);


                }

            } catch (Exception e) {

            }


            if (!(descriptionList.get(0).equals("http://ec2-52-15-107-53.us-east-2.compute.amazonaws.comnull"))) {
                bottomNavigationView.setVisibility(View.GONE);


                final String id = getYouTubeId(descriptionList.get(0).toString());

            youtubeFragment.initialize(YoutubeDeveloperKey, new YouTubePlayer.OnInitializedListener() {
                    @Override
                    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
                      youTubePlayer.setPlayerStyle(YouTubePlayer.PlayerStyle.MINIMAL);
                        youTubePlayer.cueVideo(""+id); // Plays https://www.youtube.com/watch?v=fhWaJi1Hsfo

                    }

                    @Override
                    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {

                    }
                });

                         Log.d("HEY HY","IDID "+id);

            }
        }
    }
    private String getYouTubeId (String youTubeUrl) {
        String pattern = "(?<=youtu.be/|watch\\?v=|/videos/|embed\\/)[^#\\&\\?]*";
        Pattern compiledPattern = Pattern.compile(pattern);
        Matcher matcher = compiledPattern.matcher(youTubeUrl);
        if (matcher.find()) {
            return matcher.group();
        } else {
            return "error";
        }
    }


}

