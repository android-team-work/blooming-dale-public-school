package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.media.AudioManager;
import android.media.MediaRecorder;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import android.util.Base64;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.fragment.NavHostFragment;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.google.android.material.navigation.NavigationView;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.InstanceIdResult;
import com.webtunix.bloomingdalepublicschool.R;
import com.webtunix.bloomingdalepublicschool.prelogin.SignIn;

import androidx.drawerlayout.widget.DrawerLayout;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.view.Menu;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

public class WelcomeStudent extends AppCompatActivity {

    private AppBarConfiguration mAppBarConfiguration;
    AudioManager audioManager;
    TextView navtextviewheader;
    public static final int REQUEST_ID_MULTIPLE_PERMISSIONS = 1;
    private static int SPLASH_TIME_OUT = 2000;
    private String TAG = "tag";
    int versionCode;
BottomNavigationView bottomNavigationView;
    StringBuilder subTotal,notificationBuilder;
    String token;
   int  backpress =1;
    String s1,pass,str,s4,s2,savedFCm="nul";
    PackageInfo info;
    String courseCategory;
    NavController navController;
String isSuperUser;
    ImageView imageView;
    int spinnerId = -1;
       SendRegistrationToServer sendRegistrationToServer = new SendRegistrationToServer();
    MediaRecorder recorder;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        setContentView(R.layout.activity_welcome_student2);

        if(!checkAndRequestPermissions()) {

        }

        PackageManager manager = this.getPackageManager();
        try {
             info = manager.getPackageInfo(this.getPackageName(), PackageManager.GET_ACTIVITIES);
            Log.d("Bhai mere pass","kuxh hai "+(int)info.versionCode);

        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        SharedPreferences sh = getSharedPreferences("techapppref",MODE_PRIVATE);
      final  SharedPreferences.Editor myEdit = sh.edit();
        bottomNavigationView = findViewById(R.id.bottom_navigation);
        bottomNavigationView.setItemIconTintList(null);

        Intent intent = getIntent();

      String notiFragment = intent.getStringExtra("NewNotification");

Log.d("Bhai mere pass","kuxh hai "+notiFragment);



         s1 = sh.getString("useremail", "");
        pass = sh.getString("userpassword", "");
        s4 =sh.getString("userType", "");

        savedFCm =sh.getString("FCM", "");
        Log.e("Pehla pehla fcm", "start() failed: MIC is busy   ++++ "+savedFCm.trim());


        courseCategory =sh.getString("course_category", "");


        Log.d("Getting Pass Key ",pass);



        str = s1.trim()+":"+pass.trim();

        try {
            String resVersion = new VersionCodeBackground().execute(str).get();
            Log.d("Bhai mere pass","kuxh hai "+resVersion);

            if(resVersion.trim().equals("200")){

                if( info.versionCode <  versionCode){

                    AlertDialog alertDialog = new AlertDialog.Builder(this).create();
                    alertDialog.setTitle("Application Update Alert");
                    alertDialog.setMessage("Blooming Dale Public School new version available. Please update.");
                    alertDialog.setIcon(R.mipmap.logo);
alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            SharedPreferences sharedPreferences = getSharedPreferences("techapppref", MODE_PRIVATE);

                            SharedPreferences.Editor myEdit = sharedPreferences.edit();


                            myEdit.putString("useremail","nul");
                            myEdit.putString("userpassword","nul");

                            myEdit.commit();
                            String url = "https://play.google.com/store/apps/details?id=com.webtunix.bloomingdalepublicschool";
                            Intent i = new Intent(Intent.ACTION_VIEW);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);

                            i.setData(Uri.parse(url));
                            startActivity(i);
                            finish();

                        }
                    });

                    alertDialog.show();
                }


            }
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        FirebaseInstanceId.getInstance().getInstanceId()
                .addOnCompleteListener(new OnCompleteListener<InstanceIdResult>() {
                    @Override
                    public void onComplete(@NonNull Task<InstanceIdResult> task) {
                        if (!task.isSuccessful()) {
                            Log.w("WelcomeStudent", "getInstanceId failed", task.getException());
                            return;
                        }

                        // Get new Instance ID token
                    token = task.getResult().getToken();

                        // Log and toast
                        String msg = getString(R.string.msg_token_fmt, token);
                        sendRegistrationToServer.execute(str);

                        Log.d("WelcomeStudent", token);


                            myEdit.putString("FCM",token);
                            myEdit.commit();


                        //    Toast.makeText(WelcomeStudent.this, msg, Toast.LENGTH_SHORT).show();


                    }
                });

        s2 =sh.getString("userName", "");
if(!s2.trim().equals("")){
    StringBuilder sb = new StringBuilder(s2);
    sb.setCharAt(0, Character.toUpperCase(sb.charAt(0)));
    s2= sb.toString();
}
        String s3 =sh.getString("exp_date", "");




        Toolbar toolbar = findViewById(R.id.toolbar);

        toolbar.setBackgroundResource(R.drawable.gradientcolorpalette);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);



        View hView =  navigationView.getHeaderView(0);

        imageView = (ImageView) hView.findViewById(R.id.imageViewNavBar);

        TextView nav_user = (TextView)hView.findViewById(R.id.nav_header);

        TextView nav_user_email = (TextView)hView.findViewById(R.id.nav_header_email);

     if(s4.equals("1")) {
            Menu menuNav = navigationView.getMenu();
            MenuItem nav_menuhome = menuNav.findItem(R.id.nav_home);
            MenuItem nav_menuadminhome = menuNav.findItem(R.id.nav_adminhome);
         MenuItem nav_notification = menuNav.findItem(R.id.nav_gallery);
          nav_menuhome.setTitle("Student Home");
         nav_menuadminhome.setEnabled(true).setVisible(true);
            nav_menuhome.setEnabled(true).setVisible(true);
         nav_notification.setEnabled(true).setVisible(true);

     }else if(s4.equals("0")){
            Menu menuNav = navigationView.getMenu();
            MenuItem nav_menuhome = menuNav.findItem(R.id.nav_home);
            MenuItem nav_menuadminhome = menuNav.findItem(R.id.nav_adminhome);
         MenuItem nav_notification = menuNav.findItem(R.id.nav_gallery);

         nav_menuadminhome.setEnabled(false).setVisible(false);

            nav_menuhome.setEnabled(true).setVisible(true);
         nav_notification.setEnabled(true).setVisible(true);

     }

     BottomNavigationView bottomNavigationView = (BottomNavigationView) findViewById(R.id.bottom_navigation);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
              R.id.nav_adminhome,R.id.nav_home,R.id.nav_gallery, R.id.nav_slideshow,
                R.id.nav_tools, R.id.nav_share, R.id.nav_about)
                .setDrawerLayout(drawer)
                .build();

 navController = Navigation.findNavController(this, R.id.nav_host_fragment);





if(!(notiFragment==null)) {







        new StudentDoSomething().execute(str);


}else{

    Log.d("Pending Intent","khali aaya");
}




     if(s4.equals("1")) {
    navController.navigate(R.id.nav_adminhome);


    navController.getGraph().findNode(R.id.nav_adminhome).setLabel("Welcome " + s2);

        }

        navController.getGraph().findNode(R.id.nav_home).setLabel("Welcome " + s2);

        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);

        NavigationUI.setupWithNavController(navigationView, navController);


        Bundle bundle = new Bundle();


     bottomNavigationView.setBackgroundColor(Color.rgb(237,230,230));


      bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_bottom_home:
                        Fragment navhost = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
                        NavController c = NavHostFragment.findNavController(navhost);

                        if(s4.equals("1")) {
                            c.navigate(R.id.nav_adminhome);
                            c.getGraph().findNode(R.id.nav_adminhome).setLabel("Welcome " + s2);

                        }else  if(s4.equals("0")) {
                            c.navigate(R.id.nav_home);
                            c.getGraph().findNode(R.id.nav_home).setLabel("Welcome " + s2);

                        }




                        break;
                    case R.id.nav_bottom_support:
                        Fragment navhost1 = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
                        NavController controller = NavHostFragment.findNavController(navhost1);
                        controller.navigate(R.id.nav_tools);
                        break;

                    case R.id.nav_bottom_live:
                        Fragment navhost3 = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
                        NavController controller3 = NavHostFragment.findNavController(navhost3);
                        controller3.navigate(R.id.liveclass);
                        break;


                }
                        return true;
            }
        });




        Log.d("SHARED PREF",""+s1);

        nav_user.setText(s2);

        nav_user_email.setText(s1);



        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

    }


    @Override
    protected void onResume() {
        super.onResume();

    /*    recorder = new MediaRecorder();

            boolean isMicFree = true;
     *//*   AudioManager audioManager =
                (AudioManager) getSystemService(Context.AUDIO_SERVICE);
        int id =  audioManager.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);




        if (audioManager!=null) {

            audioManager.setMicrophoneMute(true);
        }*//*

        *//*    recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
       recorder.setOutputFormat(MediaRecorder.OutputFormat.THREE_GPP);

        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AMR_NB);
        *//*

        recorder.setAudioSource(MediaRecorder.AudioSource.MIC);
        recorder.setOutputFormat(MediaRecorder.OutputFormat.MPEG_4);
        recorder.setOutputFile(Environment.getExternalStorageDirectory() + File.separator
                + Environment.DIRECTORY_DCIM + File.separator + "FILE_NAME");
        recorder.setAudioEncoder(MediaRecorder.AudioEncoder.AAC);
        recorder.setAudioChannels(1);
        recorder.setAudioSamplingRate(44100);
        recorder.setAudioEncodingBitRate(192000);
            // Configure MediaRecorder

        try {

               recorder.prepare();
            recorder.start();
        } catch (Exception e) {
            e.printStackTrace();
            Log.e("MediaRecorder", "start() failed: MIC is busy");
            explain("You need to Off your audio recorders to enjoy TECOnline. Do you want to go to app settings?");

            recorder.reset();
            recorder = null;
            // Show alert dialogs to user.
            // Ask him to stop audio record in other app.
            // Stay in pause with your streaming because MIC is busy.

            isMicFree = false;
        }

        if (isMicFree) {
            Log.e("MediaRecorder", "start() successful: MIC is free");
            // MIC is free.
            // You can resume your streaming.
        }*/

        // Do not forget to stop and release MediaRecorder for future usage
    /*   recorder.stop();
        recorder.release();
*/
        String re = null;
        try {
           re = new DeviceList().execute(str).get();
        } catch (ExecutionException e) {
            e.printStackTrace();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        if(!(savedFCm.equals(""))) {
            Log.e("ho gym  fcm", "start() failed: MIC is busy   ++++ ");

            if(!(re.equals(savedFCm))) {
                SharedPreferences sh = getSharedPreferences("techapppref",MODE_PRIVATE);

                SharedPreferences.Editor myEdit = sh.edit();


                myEdit.putString("useremail", "nul");
                myEdit.putString("userpassword", "nul");
                //myEdit.putString("FCM", "nul");


                myEdit.commit();


                Intent intent5 = new Intent(WelcomeStudent.this, SignIn.class);
                intent5.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent5);
                // Toast.makeText(SignIn.this, "You are successfully Logged Out, Thank You", Toast.LENGTH_SHORT).show();

                finish();

            }

    }else{
          Log.e(" nahi haiga fcm", "start() failed: MIC is busy");
      }



        }


    public void onWindowFocusChanged(boolean hasFocus) {
        super.onWindowFocusChanged(hasFocus);
        // Here you should do the same check with MediaRecorder.
        // And you can be sure that user does not
        // start audio recording through notifications.
        // Or user stops recording through notifications.
    }
    @Override
    protected void onPause() {
        super.onPause();


    }

    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stub
        super.onDestroy();
    }

    public void clearApplicationData() {
        File cache = getCacheDir();
        File appDir = new File(cache.getParent());
        if (appDir.exists()) {
            String[] children = appDir.list();
            for (String s : children) {
                if (!s.equals("lib")) {
                    deleteDir(new File(appDir, s));
                    Log.i("EEEEEERRRR", "**************** File /data/data/APP_PACKAGE/" + s + " DELETED *******************");
                }
            }
        }
    }

    public static boolean deleteDir(File dir) {
        if (dir != null && dir.isDirectory()) {
            String[] children = dir.list();
            for (int i = 0; i < children.length; i++) {
                boolean success = deleteDir(new File(dir, children[i]));
                if (!success) {
                    return false;
                }
            }
        }

        return dir.delete();
    }




    @Override
    public void onBackPressed(){
        final SharedPreferences sharedPreferences = getSharedPreferences("techapppref", MODE_PRIVATE);


            if (Navigation.findNavController(this,R.id.nav_host_fragment)
                    .getCurrentDestination().getId() == R.id.nav_adminhome) {
                // handle back button the way you want here


                    androidx.appcompat.app.AlertDialog alertDialog = new androidx.appcompat.app.AlertDialog.Builder(WelcomeStudent.this).create();
                    alertDialog.setTitle("Alert Message");
                    alertDialog.setMessage("Do you want to close & Logout from this application ?");
                    alertDialog.setIcon(R.mipmap.logo);





                    alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {


                            SharedPreferences.Editor myEdit = sharedPreferences.edit();


                            myEdit.putString("useremail","nul");
                            myEdit.putString("userpassword","nul");
                            myEdit.commit();

                            Intent intent = new Intent(WelcomeStudent.this, SignIn.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                          //  Toast.makeText(WelcomeStudent.this,"You are successfully Logged Out, Thank You",Toast.LENGTH_SHORT).show();

                            finish();
                        }
                    });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                    alertDialog.show();



                return;
            }else

        if (Navigation.findNavController(this,R.id.nav_host_fragment)
                .getCurrentDestination().getId() == R.id.nav_home) {
            // handle back button the way you want here

            if(!s4.equals("1")) {

                androidx.appcompat.app.AlertDialog alertDialog = new androidx.appcompat.app.AlertDialog.Builder(WelcomeStudent.this).create();
                alertDialog.setTitle("Alert Message");
                alertDialog.setMessage("Do you want to close & Logout from this application ?");
                alertDialog.setIcon(R.mipmap.logo);





                alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "YES", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {



                        SharedPreferences.Editor myEdit = sharedPreferences.edit();


                        myEdit.putString("useremail","nul");
                        myEdit.putString("userpassword","nul");

                        myEdit.commit();

                        Intent intent = new Intent(WelcomeStudent.this, SignIn.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                      //  Toast.makeText(WelcomeStudent.this,"You are successfully Logged Out, Thank You",Toast.LENGTH_SHORT).show();

                        finish();
                    }
                });
                alertDialog.setButton(AlertDialog.BUTTON_NEGATIVE, "NO", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {


                    }
                });

                alertDialog.show();


            } else{
                Navigation.findNavController(this,R.id.nav_host_fragment).navigate(R.id.nav_adminhome);


                Navigation.findNavController(this,R.id.nav_host_fragment).getGraph().findNode(R.id.nav_adminhome).setLabel("Welcome " + s2);
            }

            return;
        }





        super.onBackPressed();

    }





    public void onNewToken(String token) {
        this.token = token;
        Log.d("WelcomeStudent", "Refreshed token: " + token);
        sendRegistrationToServer.execute(str);

        // If you want to send messages to this application instance or
        // manage this apps subscriptions on the server side, send the
        // Instance ID token to your app server.
      //  sendRegistrationToServer(token);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        getMenuInflater().inflate(R.menu.welcome_student, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {

        switch (item.getItemId()) {
            case R.id.action_settings:

                Fragment navhost = getSupportFragmentManager().findFragmentById(R.id.nav_host_fragment);
                NavController c = NavHostFragment.findNavController(navhost);
                c.navigate(R.id.nav_share);


                break;

        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }



    class SendRegistrationToServer extends AsyncTask<String,Void,String> {


        @Override
        protected String doInBackground(String... strings) {


            try {

                byte[] data;
                String base64 = null;


                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                JSONObject postDataParams = new JSONObject();
                postDataParams.put("name", s1);

                postDataParams.put("registration_id", token);

              postDataParams.put("type", "android");
                URL obj = new URL(getString(R.string.server_link)+"devices");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("POST");

                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Content-Type", "application/x-www-form-urlencoded; charset=utf-8");

                con.setRequestProperty("Expect", "100-continue");
             con.addRequestProperty("Authorization", "Basic "+ base64);


               con.setDoOutput(true);
          //      con.setDoInput(true);
                OutputStream os = con.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(os, "utf-8"));

                writer.write(getPostDataString(postDataParams));
                Log.i("paramsData VINIIIII FCM", getPostDataString(postDataParams).toString());


                writer.flush();
                writer.close();
                os.close();

                int responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

       /*         InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder subTotal = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    subTotal.append(line).append('\n');
                }
                System.out.println("Response String :: " + subTotal);*/


            }catch (Exception e){

            }
            return "";

        }

        public String getPostDataString(JSONObject params) throws Exception {

            StringBuilder result = new StringBuilder();
            boolean first = true;

            Iterator<String> itr = params.keys();

            while(itr.hasNext()){

                String key= itr.next();
                Object value = params.get(key);

                if (first)
                    first = false;
                else
                    result.append("&");

                result.append(URLEncoder.encode(key, "UTF-8"));
                result.append("=");
                result.append(URLEncoder.encode(value.toString(), "UTF-8"));

            }
            Log.i("Vineet Kumar G FCM", result.toString() + "");

            return result.toString();
        }
    }


    class StudentDoSomething extends AsyncTask<String,Void,Void> {


        @Override
        protected Void doInBackground(String... strings) {

            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }

                URL obj = new URL(getString(R.string.server_link)+"Student_Notification_list");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);
                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);

                int  responseCode = con.getResponseCode();
                System.out.println("Response Code :: " + responseCode);

                Log.i("Hazoor Sir Java ", responseCode + "");

                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                notificationBuilder = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    notificationBuilder.append(line).append('\n');
                }

                Log.i("notificationBuilder", notificationBuilder + "");




            } catch (Exception e) {
                e.printStackTrace();
            }


            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            try {


                JSONObject jsonObject = new JSONObject(notificationBuilder.toString());

                JSONArray jsonArray = jsonObject.getJSONArray("data");

                int i = (jsonArray.length()-1);
                    JSONObject jsonObject2 = jsonArray.getJSONObject(i);



                    spinnerId=jsonObject2.optInt("id");

Log.d("Do youuthink ",""+spinnerId);

                Bundle bundleString = new Bundle();
                bundleString.putString("NotificationSingleID", "" + spinnerId);
                Log.d("Bhaji Spinner ID ","CHaklo CHaklo "+spinnerId);

                navController.navigate(R.id.notificationSinglePage, bundleString);



            }catch (Exception e){

            }



        }
    }
  /*  @Override
    public void onWindowFocusChanged(boolean hasFocus){
        super.onWindowFocusChanged(hasFocus);
       getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
    }*/



    class VersionCodeBackground extends AsyncTask<String,Void,String>{


        @Override
        protected String doInBackground(String... strings) {

            int  resportCode=0;


            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }
                URL obj = new URL(getString(R.string.server_link)+"111");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);
                resportCode = con.getResponseCode();
                System.out.println("Response Code :: " + resportCode);


                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
                StringBuilder devicesubTotal = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    devicesubTotal.append(line).append('\n');
                }

                Log.d("GETTING SUB USER DATA",""+devicesubTotal);
                JSONObject jsonObject = new JSONObject(devicesubTotal.toString());

                versionCode =  jsonObject.optInt("version");

                Log.d("GETTING Registration ID",""+versionCode);



            } catch (Exception e) {
                e.printStackTrace();
            }


            return resportCode+"";

        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                //JSONObject jsonObject = new JSONObject(subTotal.toString());



              /*  aVoid.add(jsonObject.optString("username"));
                aVoid.add(jsonObject.optString("exp_date"));*/






            }catch (Exception e){

            }






        }
    }

    private  boolean checkAndRequestPermissions() {
        int camerapermission = ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA);
        int writepermission = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        int permissionLocation = ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION);
        int permissionRecordAudio = ContextCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO);


        List<String> listPermissionsNeeded = new ArrayList<>();

        if (camerapermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CAMERA);
        }
        if (writepermission != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
        }

      /*  if (permissionRecordAudio != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
        }*/
        if (!listPermissionsNeeded.isEmpty()) {
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), REQUEST_ID_MULTIPLE_PERMISSIONS);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        Log.d("TAG", "Permission callback called-------");
        switch (requestCode) {
            case REQUEST_ID_MULTIPLE_PERMISSIONS: {

                Map<String, Integer> perms = new HashMap<>();
                // Initialize the map with both permissions
                perms.put(Manifest.permission.CAMERA, PackageManager.PERMISSION_GRANTED);
                perms.put(Manifest.permission.WRITE_EXTERNAL_STORAGE, PackageManager.PERMISSION_GRANTED);
                // Fill with actual results from user
                if (grantResults.length > 0) {
                    for (int i = 0; i < permissions.length; i++)
                        perms.put(permissions[i], grantResults[i]);
                    // Check for both permissions
                    if (perms.get(Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                            && perms.get(Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

                           ) {
                        Log.d("TAG", "sms & location services permission granted");
                        // process the normal flow

                        //else any one or both the permissions are not granted
                    } else {
                        Log.d("TAG", "Some permissions are not granted ask again ");
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
//                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA)
                                || ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                             ) {
                            showDialogOK("Service Permissions are required for this app",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialog, int which) {
                                            switch (which) {
                                                case DialogInterface.BUTTON_POSITIVE:
                                                    checkAndRequestPermissions();
                                                    break;
                                                case DialogInterface.BUTTON_NEGATIVE:
                                                    // proceed with logic by disabling the related features or quit the app.
                                                    finish();
                                                    break;
                                            }
                                        }
                                    });
                        }
                        //permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                        else {
                            explain("You need to give some mandatory permissions to continue. Do you want to go to app settings?");
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }
                    }
                }
            }
        }

    }

    private void showDialogOK(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(this)
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", okListener)
                .create()
                .show();
    }
    private void explain(String msg){
        final AlertDialog.Builder dialog = new AlertDialog.Builder(this);
        dialog.setMessage(msg)
                .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        //  permissionsclass.requestPermission(type,code);

                        Intent intent = new Intent(android.provider.Settings.ACTION_SETTINGS);
                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                       startActivity(intent);                  }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                        finish();
                    }
                });
        dialog.show();
    }

    class DeviceList extends AsyncTask<String,Void,String>{

        String deviceId;
        @Override
        protected String doInBackground(String... strings) {

            int  resportCode=0;


            try {

                byte[] data;
                String base64=null;



                try {

                    data = strings[0].getBytes("UTF-8");

                    base64 = Base64.encodeToString(data, Base64.DEFAULT);

                    Log.i("Base  ", base64);

                } catch (Exception e) {

                    e.printStackTrace();

                }
                URL obj = new URL(getString(R.string.server_link)+"User_Device_list");

                HttpURLConnection con = (HttpURLConnection) obj.openConnection();
                con.setRequestMethod("GET");
                con.setReadTimeout(20000);
                con.setConnectTimeout(20000);

                con.setRequestProperty("Content-Type", "application/json; charset=utf-8");
                con.setRequestProperty("Expect", "100-continue");
                con.addRequestProperty("Authorization", "Basic "+base64);
                resportCode = con.getResponseCode();
                System.out.println("Response Code :: " + resportCode);


                InputStream inputStream = con.getInputStream();

                BufferedReader r = new BufferedReader(new InputStreamReader(inputStream));
               StringBuilder devicesubTotal = new StringBuilder();
                for (String line; (line = r.readLine()) != null; ) {
                    devicesubTotal.append(line).append('\n');
                }

                Log.d("GETTING SUB USER DATA",""+devicesubTotal);
                JSONObject jsonObject = new JSONObject(devicesubTotal.toString());

                JSONObject jsonObject1 = jsonObject.getJSONObject("data");
            deviceId = (jsonObject1.optString("registration_id"));

                Log.d("GETTING Registration ID",""+deviceId);



            } catch (Exception e) {
                e.printStackTrace();
            }


            return deviceId+"";

        }

        @Override
        protected void onPostExecute(String aVoid) {
            super.onPostExecute(aVoid);
            try {
                //JSONObject jsonObject = new JSONObject(subTotal.toString());



              /*  aVoid.add(jsonObject.optString("username"));
                aVoid.add(jsonObject.optString("exp_date"));*/






            }catch (Exception e){

            }






        }


    }

}
