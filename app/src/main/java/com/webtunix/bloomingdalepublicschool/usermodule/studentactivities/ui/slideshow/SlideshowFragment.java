package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.slideshow;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;

import com.webtunix.bloomingdalepublicschool.R;


public class SlideshowFragment extends Fragment {

    Button button;
    String pdfMode="nul";
    View root;
    private SlideshowViewModel slideshowViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
      /*  getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);*/
      //  SharedPreferences sh = getActivity().getSharedPreferences("techapppPDF",getContext().MODE_PRIVATE);

       SharedPreferences sh = getActivity().getSharedPreferences("techapppPDF",Context.MODE_PRIVATE);

        pdfMode = sh.getString("pdfMode","");




      root = inflater.inflate(R.layout.fragment_slideshow, container, false);

        button = root.findViewById(R.id.pdfnightModebtn);

        if(pdfMode.isEmpty()){
           pdfMode = button.getText().toString().trim();
        }

button.setText(pdfMode);
        //Log.d("PDF setting pehla",pdfMode);




        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("PDF setting",pdfMode);

        if(button.getText().toString().equals("OFF")){

                    button.setText("ON");

                    pdfMode = "ON";
                    SharedPreferences sharedPreferences
 = getActivity().getSharedPreferences("techapppPDF",Context.MODE_PRIVATE);

                    SharedPreferences.Editor myEdit = sharedPreferences.edit();
                    myEdit.putString("pdfMode",pdfMode);

                    myEdit.commit();
                    Log.d("PDF setting",pdfMode);

                }else if(button.getText().toString().equals("ON")){
                    button.setText("OFF");
                    pdfMode = "OFF";
                    Log.d("PDF setting",pdfMode);

                    SharedPreferences sharedPreferences
 = getActivity().getSharedPreferences("techapppPDF",Context.MODE_PRIVATE);

                    SharedPreferences.Editor myEdit = sharedPreferences.edit();
                    myEdit.putString("pdfMode",pdfMode);

                    myEdit.commit();
                }
         }
        });

 /*pdfMode = button.getText().toString();


        myEdit.putString("pdfMode",pdfMode);

        myEdit.commit();*/

        return root;
    }
}