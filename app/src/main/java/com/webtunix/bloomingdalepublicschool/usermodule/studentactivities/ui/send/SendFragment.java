package com.webtunix.bloomingdalepublicschool.usermodule.studentactivities.ui.send;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.ViewModelProviders;

import com.webtunix.bloomingdalepublicschool.R;


public class SendFragment extends Fragment {



    private SendViewModel sendViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        getActivity().getWindow().setFlags(WindowManager.LayoutParams.FLAG_SECURE,
                WindowManager.LayoutParams.FLAG_SECURE);
        sendViewModel =
                ViewModelProviders.of(this).get(SendViewModel.class);
        View root = inflater.inflate(R.layout.fragment_send, container, false);


        TextView textView = root.findViewById(R.id.text_send);



        textView.setText("Blooming Dale Public School kindles the curiosity and lasting passion for learning in students while preparing them for life.  Ms. Veena, as the principal of this institute truly believes in " +
                "vision of illuminating the minds of children with creativity and right" +
                " knowledge to shape a better future.   Our co-education and English medium school at Baltana (Distt. Mohali) is recognized by Chandigarh Education Department. We take pride in delivering the best of education with an experienced faculty that serves knowledge at its best. \n" +
                "Our main objective is the development of students as a whole," +
                " to instill intellectual, moral and spiritual values. We encourage children to be actively and " +
                "positively competent, aware and concerned about national development. " +
                "We aim at culturing the pupils into responsible members of society. And we work practically, responsibly and diligently towards making it " +
                "happen.Type a message\n" +
                "");

            return root;
    }


}
